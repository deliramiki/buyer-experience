import { SITE_URL } from './constants';

const markdownIt = require('markdown-it')({
  html: true,
  linkify: true,
});

/**
 * Utility function to generate Faq component markup schema
 *
 * @param {any} faqSchemaData - An object containing the faqSchemaData information
 */
export const createSchemaFaq = (faqSchemaData: any) => {
  const entities = faqSchemaData.map(
    (schema: { question: any; answer: any; cta: { url: any; text: any } }) => {
      return {
        '@type': 'Question',
        name: `<p>${schema.question}</p>`,
        acceptedAnswer: {
          '@type': 'Answer',
          text: markdownIt.render(schema.answer),
        },
      };
    },
  );

  return {
    '@context': 'http://schema.org',
    '@type': 'FAQPage',
    mainEntity: entities,
  };
};

/**
 * Utility function to generate Breadcrumb component markup schema
 *
 * @param {Array} crumbs - A collection of crumb items
 */

export const createSchemaBreadcrumb = (crumbs: any) => {
  const items = crumbs.map((item: any, index: number) => {
    return {
      '@type': 'ListItem',
      position: index + 1,
      name: item.title,
      item: item.href
        ? `https://about.gitlab.com${item.href}`
        : window.location.href,
    };
  });
  return {
    '@context': 'https://schema.org/',
    '@type': 'BreadcrumbList',
    itemListElement: [...items],
  };
};

/**
 * Utility function to generate WebPage markup schema
 *
 * @param {string} title
 * @param {string} description
 * @param {string} url
 * @param articleSection
 * @param {string} timeRequired
 * @param datePublished
 * @param dateModified
 */
export const createSchemaPage = ({
  title,
  description,
  url,
  articleSection,
  timeRequired,
  datePublished,
  dateModified,
}: {
  title?: string;
  description?: string;
  url?: string;
  articleSection?: any;
  timeRequired?: string;
  datePublished?: string;
  dateModified?: string;
}) => {
  const schema = {
    '@context': 'https://schema.org',
    '@type': 'Article',
    mainEntityOfPage: {
      '@type': 'WebPage',
      '@id': url,
    },
    headline: title,
    description,
    image: `${SITE_URL}/nuxt-images/resources/fallback/img-fallback-cards-infinity.png`,
    articleSection,
    timeRequired,
    author: {
      '@type': 'Organization',
      name: 'GitLab',
      url: 'https://about.gitlab.com/',
    },
    publisher: {
      '@type': 'Organization',
      name: 'GitLab',
      logo: {
        '@type': 'ImageObject',
        url: 'https://about.gitlab.com/images/press/logo/png/gitlab-logo-500.png',
      },
    },
    datePublished,
    dateModified,
  };

  return schema;
};
