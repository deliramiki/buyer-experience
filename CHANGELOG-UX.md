## [1.0.1](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui/-/issues/278) Minimal footer update
**Mar 13, 2023 - Mar 24, 2023**

### Updated

* Removed social icons from minimal footer

## 1.0.0 UX Changelog Naming Convention 
**Feb 13, 2023 - Feb 26, 2023**

### ♻️ added

* Create a [design changelog](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/merge_requests/1709/)
