import { Context } from '@nuxt/types';
import {
  GetStartedService,
  WhyGitLabService,
  SolutionsService,
  FreeTrialService,
  SolutionsStartupsService,
  SolutionsNonprofitService,
  SecurityService,
  InstallService,
  ContentfulService,
  IntegrationsService,
  EnvironmentalSocialGovernanceService,
} from '../services';

export default ($ctx: Context, inject: any) => {
  const whyGitlabService = new WhyGitLabService($ctx);
  const getStartedService = new GetStartedService($ctx);
  const freeTrialService = new FreeTrialService($ctx);
  const securityService = new SecurityService($ctx);
  const solutionsService = new SolutionsService($ctx);
  const solutionsStartupsService = new SolutionsStartupsService($ctx);
  const solutionsNonprofitService = new SolutionsNonprofitService($ctx);
  const installService = new InstallService($ctx);
  const contentfulService = new ContentfulService($ctx);
  const integrationsService = new IntegrationsService($ctx);
  const environmentalSocialGovernanceService =
    new EnvironmentalSocialGovernanceService($ctx);

  inject('whyGitlabService', whyGitlabService);
  inject('getStartedService', getStartedService);
  inject('freeTrialService', freeTrialService);
  inject('securityService', securityService);
  inject('solutionsService', solutionsService);
  inject('solutionsStartupsService', solutionsStartupsService);
  inject('solutionsNonprofitService', solutionsNonprofitService);
  inject('installService', installService);
  inject('contentfulService', contentfulService);
  inject('integrationsService', integrationsService);
  inject(
    'environmentalSocialGovernanceService',
    environmentalSocialGovernanceService,
  );
};
