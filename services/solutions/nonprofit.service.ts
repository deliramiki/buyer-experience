import { Context } from '@nuxt/types';
import { CONTENT_TYPES } from '~/common/content-types';
import { COMPONENT_NAMES } from '~/common/constants';
import { getClient } from '~/plugins/contentful.js';

export class SolutionsNonprofitService {
  private readonly $context: Context;
  private $isIndex: boolean;

  constructor($ctx) {
    this.$context = $ctx;
    this.$isIndex = false;
  }

  private readonly componentNames = {
    BY_SOLUTION_VALUE_PROP: COMPONENT_NAMES.BY_SOLUTION_VALUE_PROP,
    TESTIMONIALS: 'testimonials',
    SINGLE_CTA_BLOCK: COMPONENT_NAMES.SINGLE_CTA_BLOCK,
    HERO: COMPONENT_NAMES.SOLUTIONS_HERO,
    OVERVIEW: 'nonprofit-intro',
    SIDE_NAVIGATION_VARIANT: COMPONENT_NAMES.SIDE_NAVIGATION_VARIANT,
    QUOTES_CAROUSEL: COMPONENT_NAMES.QUOTES_CAROUSEL,
    NONPROFIT_OVERVIEW: 'nonprofit-overview',
    FAQ: COMPONENT_NAMES.FAQ,
    FORM: 'open-source-form-section',
  };

  private readonly componentInternalNames = {
    SINGLE_CTA_BLOCK: 'Solutions - Nonprofit CTA Block',
    OVERVIEW: 'Solutions - Nonprofit intro',
    TESTIMONIALS: 'Solutions - Nonprofit Quotes Carousel',
    BENEFITS: 'Solutions - Nonprofit By solution value prop',
  };

  /**
   * Main method that returns components to the /solutions/nonprofit/* pages
   * @param slug
   * @param isIndex
   */
  async getContent(slug: string, isIndex: boolean = false) {
    // Default locale data - Contentful
    // Solutions Nonprofit pages ARE NOT translated yet

    this.$isIndex = isIndex;
    try {
      // FOR now, landing Nonprofit page has sideNav,
      // child pages DO NOT have sideNav

      return await this.getContentfulData(slug);
    } catch (e) {
      throw new Error(e);
    }
  }

  private async getContentfulData(_slug: string) {
    try {
      const content = await getClient().getEntries({
        content_type: CONTENT_TYPES.PAGE,
        'fields.slug': _slug,
        include: 4,
      });

      if (content.items.length === 0) {
        throw new Error('Not found');
      }

      const [solutions] = content.items;

      return this.transformContentfulData(solutions);
    } catch (e) {
      throw new Error(e);
    }
  }

  private async transformContentfulData(ctfData: any) {
    const { pageContent, seoMetadata } = ctfData.fields;

    const mappedContent = this.getPageComponents(pageContent);

    if (this.$isIndex) {
      const [sideNav]: any = mappedContent.filter(
        (component) =>
          component.name === this.componentNames.SIDE_NAVIGATION_VARIANT,
      );

      const [hero]: any = mappedContent.filter(
        (component) => component.name === this.componentNames.HERO,
      );

      const [ctaBlock]: any = mappedContent.filter(
        (component) => component.name === this.componentNames.SINGLE_CTA_BLOCK,
      );

      const otherComponents = mappedContent.filter(
        (component) =>
          component.name !== this.componentNames.HERO &&
          component.name !== this.componentNames.SINGLE_CTA_BLOCK &&
          component.name !== this.componentNames.SIDE_NAVIGATION_VARIANT,
      );

      const newSideNav = {
        name: this.componentNames.SIDE_NAVIGATION_VARIANT,
        slot_enabled: true,
        links: sideNav.links,
        slot_content: [...otherComponents],
      };

      const newArray = [];

      newArray.push(hero);
      newArray.push(newSideNav);
      newArray.push(ctaBlock);

      const transformedPageWithSideNav = {
        ...seoMetadata[0]?.fields,
        title: seoMetadata[0]?.fields.ogTitle,
        next_step_alt: true,
        no_gradient: true,
        components: newArray,
      };

      return transformedPageWithSideNav;
    }

    const transformedPage = {
      ...seoMetadata[0]?.fields,
      title: seoMetadata[0]?.fields.ogTitle,
      pageContent,
      components: mappedContent,
      next_step_alt: true,
      template: 'industry',
    };

    return transformedPage;
  }

  private getPageComponents(pageContent: any[]) {
    const components: any[] = [];

    pageContent.forEach((ctfComponent) => {
      const mappedComponent = this.mapCtfComponent(ctfComponent);
      components.push(mappedComponent);
    });

    return components;
  }

  private mapCtfComponent(ctfComponent: any) {
    let component;

    const { id } = ctfComponent.sys.contentType.sys;

    switch (id) {
      case CONTENT_TYPES.HERO: {
        component = this.mapHero(ctfComponent);
        break;
      }

      case CONTENT_TYPES.SIDE_MENU: {
        component =
          this.$context.$solutionsStartupsService.mapSideNavigation(
            ctfComponent,
          );
        break;
      }

      case CONTENT_TYPES.HEADER_AND_TEXT: {
        component = this.mapHeaderAndText(ctfComponent);
        break;
      }

      case CONTENT_TYPES.CARD_GROUP: {
        component = this.mapCardGroup(ctfComponent);
        break;
      }

      case CONTENT_TYPES.TWO_COLUMN_BLOCK: {
        component = this.mapForm(ctfComponent);
        break;
      }

      case CONTENT_TYPES.FAQ: {
        component = this.mapFaq(ctfComponent);
        break;
      }

      default:
        break;
    }

    return component;
  }

  private mapHero(ctfHero: any) {
    const {
      title,
      subheader,
      description,
      primaryCta,
      secondaryCta,
      backgroundImage,
      customFields,
    } = ctfHero.fields;

    if (!this.$isIndex) {
      return {
        name: this.componentNames.HERO,
        data: {
          title,
          subtitle: subheader,
          footnote: description,
          primary_btn: {
            url: primaryCta.fields.externalUrl,
            text: primaryCta.fields.text,
            data_ga_name: primaryCta.fields.dataGaName,
            data_ga_location: primaryCta.fields.dataGaLocation,
            icon: {
              name: primaryCta.fields.iconName,
              variant: primaryCta.fields.iconVariant,
            },
          },
          image: {
            image_url: `${backgroundImage.fields.file.url}?q=10`,
            hide_in_mobile: true,
            alt: '',
          },
          ...customFields,
        },
      };
    }

    return {
      name: this.componentNames.HERO,
      data: {
        title,
        subtitle: subheader,
        aos_animation: 'fade-down',
        aos_duration: 500,
        aos_offset: 200,
        img_animation: 'zoom-out-left',
        img_animation_duration: 1600,
        primary_btn: {
          url: primaryCta.fields.externalUrl,
          text: primaryCta.fields.text,
          data_ga_name: primaryCta.fields.dataGaName,
          data_ga_location: primaryCta.fields.dataGaLocation,
        },
        secondary_btn: {
          url: secondaryCta.fields.externalUrl,
          text: secondaryCta.fields.text,
          data_ga_name: secondaryCta.fields.dataGaName,
          data_ga_location: secondaryCta.fields.dataGaLocation,
          icon: {
            name: secondaryCta.fields.iconName,
            variant: secondaryCta.fields.iconVariant,
          },
        },
        image: {
          bordered: true,
          image_purple_background: true,
          image_url: `${backgroundImage.fields.file.url}?q=10`,
          image_url_mobile: `${backgroundImage.fields.file.url}?q=50`,
          alt: '',
        },
      },
    };
  }

  private mapHeaderAndText(ctfHeaderAndText: any) {
    const { text, componentName } = ctfHeaderAndText.fields;

    return {
      name: 'div',
      id: componentName || 'overview',
      slot_enabled: true,
      slot_content: [
        {
          name: this.componentNames.OVERVIEW,
          data: {
            text: {
              description: text,
            },
          },
        },
      ],
    };
  }

  private mapCardGroup(ctfCardGroup: any) {
    let component;

    switch (ctfCardGroup.fields.internalName.trim()) {
      case this.componentInternalNames.TESTIMONIALS:
        component = this.mapCaseStudyCarousel(ctfCardGroup);
        break;

      case this.componentNames.NONPROFIT_OVERVIEW:
        component = this.mapNonprofitOverview(ctfCardGroup);
        break;

      case this.componentInternalNames.SINGLE_CTA_BLOCK:
        // We pass true as second argument to indicate this is a Single CTA Block
        component = this.$context.$solutionsStartupsService.mapCtaBlock(
          ctfCardGroup,
          true,
        );
        break;

      case this.componentInternalNames.BENEFITS:
        component =
          this.$context.$solutionsStartupsService.mapBySolutionValueProp(
            ctfCardGroup,
          );
        break;

      default:
        break;
    }
    return component;
  }

  private mapCaseStudyCarousel(ctfCaseStudyCarousel: any) {
    const { header, card: cards, componentName } = ctfCaseStudyCarousel.fields;
    return {
      name: 'div',
      id: componentName,
      slot_enabled: true,
      slot_content: [
        {
          name: this.componentNames.QUOTES_CAROUSEL,
          data: {
            header,
            case_studies: cards.map((card) => {
              return {
                logo_url: `${card.fields.image.fields.file.url}?q=80`,
                instituion_name:
                  card.fields.contentArea[0].fields.authorCompany,
                quote: {
                  quote_text: card.fields.contentArea[0].fields.quoteText,
                  author: card.fields.contentArea[0].fields.author,
                  author_title: card.fields.contentArea[0].fields.authorTitle,
                },
              };
            }),
          },
        },
      ],
    };
  }

  private mapNonprofitOverview(ctfNonprofitOverview: any) {
    const { description, card: cards } = ctfNonprofitOverview.fields;

    const blocks = [];

    const firstBlockHeader = cards.find(
      (card) => card.sys.id === '6Tt4rZbKHZXh0QervjNVja',
    );

    // BUILD FIRST BLOCK
    const newFirstBlock: any = {
      title: firstBlockHeader.fields.title,
      offers: [{ list: firstBlockHeader.fields.list }],
    };

    blocks.push(newFirstBlock);

    // FIND ADDITIONAL REQUIREMENTS SECTION
    const [additionalRequirements] = cards.filter(
      (entry) => entry.sys.id === '19axL4IlF7auuCrIIVp5xU',
    );

    const informationBlock = {
      title: additionalRequirements.fields.title,
      text: additionalRequirements.fields.list,
      link: {
        url: additionalRequirements.fields.button.fields.externalUrl,
      },
    };

    const result = {
      name: this.componentNames.NONPROFIT_OVERVIEW,
      data: {
        blocks,
        footnote: description,
        information: informationBlock,
      },
    };

    return result;
  }

  private mapForm(ctfForm: any) {
    const { header, componentId, blockGroup } = ctfForm.fields;
    const form = blockGroup[0].fields;
    const blocks = blockGroup.slice(1).map((block) => {
      const { header, text } = block.fields;
      if (header) {
        return {
          title: header,
          content: text,
        };
      }

      return {
        content: text,
      };
    });

    return {
      name: this.componentNames.FORM,
      data: {
        title: header,
        id: componentId,
        blocks,
        form: {
          formId: form.formId,
          form_header: '',
          datalayer: form.formDataLayer,
        },
      },
    };
  }

  private mapFaq(ctfFaq: any) {
    const component = this.$context.$freeTrialService.mapFaq(ctfFaq);

    return {
      name: this.componentNames.FAQ,
      data: { ...component, normal_header: true, extra_margin: true },
    };
  }
}
