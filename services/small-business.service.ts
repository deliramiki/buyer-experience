export function smallBusinessDataHelper(items: any) {
  //  Data Mapping
  const data = items[0].fields.pageContent;
  const metadata = items[0].fields.seoMetadata[0];
  const [hero, navLinks, valueProp, caseStudies, card, pricing] = data;

  const doc = {} as any;

  // Hero Component:
  doc.solutions_hero = {
    title: hero.fields.title,
    subtitle: hero.fields.subheader,
    image: {
      image_url: hero.fields.backgroundImage.fields.file.url,
      alt: 'Small business Header image',
      bordered: true,
    },
    primary_btn: {
      text: hero.fields.primaryCta.fields.text,
      url: hero.fields.primaryCta.fields.externalUrl,
      data_ga_name: hero.fields.primaryCta.fields.dataGaName,
      data_ga_location: 'hero',
    },
    secondary_btn: {
      text: hero.fields.secondaryCta.fields.text,
      url: hero.fields.secondaryCta.fields.externalUrl,
      data_ga_name: hero.fields.secondaryCta.fields.dataGaName,
      data_ga_location: 'hero',
    },
  };

  // Side Navigation
  doc.side_navigation_links = navLinks.fields.anchors.map((link: any) => ({
    href: link.fields.anchorLink,
    title: link.fields.linkText,
  }));

  // Value prop

  doc.by_solution_value_prop = {
    title: valueProp.fields.header,
    light_background: true,
    full_header: true,
    last_card_gradient: true,
    gradient_cta: true,
    cards: valueProp.fields.card.map((card: any) => ({
      title: card.fields.title,
      description: card.fields.description,
      href: card.fields.cardLink,
      pill: card.fields.pills,
      data_ga_name: card.fields.title,
      icon: {
        name: card.fields.iconName,
        alt: `${card.fields.iconName} icon`,
        variant: 'marketing',
      },
    })),
  };

  // Case studies
  doc.by_industry_case_studies = {
    title: caseStudies.fields.header,
    link: {
      text: 'All case studies',
    },
    rows: caseStudies.fields.card.map((card: any) => ({
      title: card.fields.title,
      subtitle: card.fields.subtitle,
      description: card.fields.description,
      image: {
        alt: card.fields.image.fields.title,
        url: card.fields.image.fields.file.url,
      },
      button: {
        text: 'Read Case Study',
        href: card.fields.button.fields.externalUrl,
        data_ga_name: `${card.fields.title}`,
        data_ga_location: 'body',
      },
    })),
  };

  // Card
  doc.card_content = {
    title: card.fields.title,
    description: card.fields.description,
    button: {
      text: 'Learn More about GitLab’s Startup Program',
      href: card.fields.cardLink,
      data_ga_name: card.fields.button.fields.dataGaName,
      data_ga_location: 'body',
    },
  };

  doc.pricing_tier = {
    title: pricing.fields.header,
    subtitle: pricing.fields.cta.fields.text,
    description: pricing.fields.description,
    cards: pricing.fields.card,
  };

  // Metadata
  doc.title = metadata.fields.ogTitle;
  doc.ogTitle = metadata.fields.ogTitle;
  doc.description = metadata.fields.description;
  doc.ogDescription = metadata.fields.ogDescription;
  doc.ogType = metadata.fields.ogType;
  doc.ogUrl = metadata.fields.ogUrl;
  doc.ogSiteName = metadata.fields.ogSiteName;
  doc.ogImage = metadata.fields.ogImage;

  return doc;
}
