interface PrivacyPageData {
  side_menu: {
    anchors?: {
      text: string;
      data: [];
    };
    hyperlinks?: {};
  };
  hero_title: string;
  last_updated: string;
  copy?: string;
  disclaimer?: {};
  sections: {}[];
}

export function privacyDataHelper(data: any[]) {
  let pageData: PrivacyPageData = {
    side_menu: {
      anchors: {
        text: '',
        data: [],
      },
      hyperlinks: {},
    },
    hero_title: '',
    last_updated: '',
    copy: '',
    sections: [],
  };

  // Hero
  const heroData = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'eventHero',
  );

  (pageData.hero_title = heroData[0].fields.title),
    (pageData.last_updated = heroData[0].fields.subheader),
    (pageData.copy = heroData[0].fields.description);

  // Disclaimer (only used on FR, DE, JA)
  const disclaimer = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'headerAndText',
  );

  if (disclaimer.length > 0) {
    pageData.disclaimer = {
      text: disclaimer[0]?.fields.header,
      details: disclaimer[0]?.fields.text,
    };
  }

  // Side Menu
  const navData = data.filter(
    (obj) => obj.sys.contentType.sys.id === 'sideMenu',
  );

  pageData.side_menu = {
    anchors: {
      text: navData[0].fields.header,
      data: navData[0].fields.anchors.map((anchor) => {
        return {
          text: anchor.fields.linkText,
          href: anchor.fields.anchorLink,
          data_ga_name: anchor.fields.dataGaName,
          data_ga_location: anchor.fields.dataGaLocation,
        };
      }),
    },
    hyperlinks: {},
  };

  // Side Menu Content
  pageData.sections = navData[0].fields.content.map((section) => {
    return {
      header: section.fields.header,
      id: section.fields.headerAnchorId,
      text: section.fields.text,
    };
  });

  return pageData;
}
