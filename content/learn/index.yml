---
  title: "GitLab Learn"
  description: "Learn new skills while working with GitLab. Our easy-to-use learning platform provides instructions and feedback throughout your journey."
  certifications_link:
    title: "Learn more about GitLab Certifications"
    link: "https://levelup.gitlab.com/pages/certifications/"
    data_ga_name: "learn more"
    data_ga_location: "header"
  free_trial_link:
    title: "Get free trial"
    link: "https://gitlab.com/-/trial_registrations/new?_gl=1%2Aiun8xs%2A_ga%2AMjYyMTI5MzcuMTYyNjk3NTQwMg..%2A_ga_ENFH3X7M5Y%2AMTY2MzY5MTAzNi43Mi4wLjE2NjM2OTEwMzYuMC4wLjA.&glm_content=default-saas-trial&glm_source=about.gitlab.com"
    data_ga_name: "free trial"
    data_ga_location: "header"
  reset_all: "Reset all"
  use_case: "Use-case"
  level: "Level"
  pace: "Self-paced"
  assessment: "Assessment"
  registration: "Registration"
  confidentiality: "Confidentiality"
  maintainer: "Maintainer"
  programs:
    - name: "TeamOps Certification"
      url: https://about.gitlab.com/teamops/
      description: |
        TeamOps is a new people practice that brings precision and operations to how people work together.
      live_date: "2022-10-01"
      maintainer: "Learning & Development"
      level: "Intermediate"
      use_case: "Talent & Development"
      pace: "Self-Paced"
      assessment: "Certification"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab with Git Essentials Course"
      url: https://levelup.gitlab.com/courses/gitlab-with-git-essentials-s2
      description: This course will introduce you to the GitLab system and how it uses Git to transform your DevOps processes. You will learn the basics of navigating GitLab and using Git on your local computer and remotely.
      live_date: "2022-12-29"
      maintainer: "Education Services"
      level: "Beginner"
      use_case: "GitLab Basics"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab Agile Project Management Course"
      url: https://levelup.gitlab.com/learn/course/gitlab-agile-project-management
      description: This course introduces users to GitLab's Plan stage, where they can manage software products or other projects. It focuses on the various tools available, including issues, epics, milestones, iterations, labels, roadmaps, burndown charts, and boards.
      live_date: "2022-12-29"
      maintainer: "Education Services"
      level: "Intermediate"
      use_case: "Agile Planning"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab CI/CD Course"
      url: https://levelup.gitlab.com/learn/course/continuous-integration-and-delivery-ci-cd-with-gitlab
      description: This course explains what Continuous Integration/Continuous Deployment (CI/CD) pipelines are and what value they bring to the software development lifecycle. It also outlines the architecture behind GitLab's CI/CD pipelines and explains how to set up basic CI/CD pipelines in your own projects. Finally, it touches on a handful of specific CI/CD pipeline use cases, including pushing your app to a Docker Container registry and using one of GitLab's security scanners.
      live_date: "2022-12-29"
      maintainer: "Education Services"
      level: "Intermediate"
      use_case: "Continuous Integration"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab Security Essentials Course"
      url: https://levelup.gitlab.com/courses/gitlab-security-essentials-s2
      description: This course covers all of the essential security capabilities of GitLab, including Static Application Security Testing, secret detection, Dynamic Application Security Testing, dependency scanning, container scanning, license compliance, and fuzz testing.
      live_date: "2022-12-29"
      maintainer: "Education Services"
      level: "Intermediate"
      use_case: "DevSecOps"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab Certified Git Associate"
      url: https://levelup.gitlab.com/courses/gitlab-with-git-essentials-certification-exam
      description: GitLab Certified Git Associate is a technical certification offered by GitLab Education Services to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work.
      live_date: "2023-08-23"
      maintainer: "Education Services"
      level: "Beginner"
      use_case: "DevOps"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified CI/CD Associate Exam"
      url: https://levelup.gitlab.com/courses/gitlab-ci-cd-certification-exam
      description: GitLab Certified CI/CD Associate is a technical certification offered by GitLab Education Services to help the GitLab community and team members validate their ability to apply GitLab concurrent DevOps processes and best practices in their daily work.
      live_date: "2023-08-23"
      maintainer: "Education Services"
      level: "Intermediate"
      use_case: "Continuous Integration"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified Project Management Associate Exam"
      url: https://levelup.gitlab.com/courses/gitlab-project-management-certification-exam
      description: GitLab Certified Project Management Associate is a technical certification offered by GitLab Education Services to help the GitLab community and team members validate their ability to use GitLab for lean and agile project management, from basic issue tracking to scrum and kanban style project management throughout the complete DevOps lifecycle.
      live_date: "2023-08-23"
      maintainer: "Education Services"
      level: "Intermediate"
      use_case: "Agile Planning"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified Security Specialist Exam"
      url: https://levelup.gitlab.com/courses/gitlab-security-essentials-certification-exam
      description: GitLab Certified Security Specialist is a technical certification offered by GitLab Education Services to help the GitLab community and team members validate their ability to use GitLab for secure software development through continuous security testing, scanning, and remediation.
      live_date: "2023-08-23"
      maintainer: "Education Services"
      level: "Intermediate"
      use_case: "DevSecOps"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab with Git Basics Training"
      url: https://about.gitlab.com/services/education/gitlab-basics/
      description: This class provides users with an introduction to GitLab and Git. It starts with an overview of GitLab so you can learn the basics about what GitLab does and why DevOps teams use it.
      live_date: "2020-01-01"
      maintainer: "Education Services"
      level: "Beginner"
      use_case: "GitLab Basics"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab CI/CD Training"
      url: https://about.gitlab.com/services/education/gitlab-ci/
      description: This class explains what Continuous Integration/Continuous Deployment (CI/CD) pipelines are and what value they bring to the software development lifecycle. It also outlines the architecture behind GitLab's CI/CD pipelines and explains how to set up basic CI/CD pipelines in your own projects.
      live_date: "2020-01-01"
      maintainer: "Education Services"
      level: "Intermediate"
      use_case: "Continuous Delivery/Release"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab for Project Managers Training"
      url: https://about.gitlab.com/services/education/pm/
      description: This class introduces users to GitLab's Plan stage, where they can manage software products or other projects. It focuses on the various tools available, including issues, epics, milestones, iterations, labels, roadmaps, burndown charts, and boards.
      live_date: "2020-01-01"
      maintainer: "Education Services"
      level: "Intermediate"
      use_case: "Agile Planning"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Security Essentials Training"
      url: https://about.gitlab.com/services/education/security-essentials/
      description: This course covers all of the essential security capabilities of GitLab, including Static Application Security Testing, secret detection, Dynamic Application Security Testing, dependency scanning, container scanning, license compliance, and fuzz testing.
      live_date: "2020-12-01"
      maintainer: "Education Services"
      level: "Intermediate"
      use_case: "DevSecOps"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab DevOps Fundamentals Training"
      url: https://about.gitlab.com/services/education/devops-fundamentals/
      description: Concurrent DevOps is a new way of thinking about how to create and ship software. Rather than organizing work in a sequence of steps and handoffs, the power of working concurrently is in unleashing collaboration across the organization.
      live_date: "2020-01-01"
      maintainer: "Education Services"
      level: "Advanced"
      use_case: "DevOps"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab System Administration Training"
      url: https://about.gitlab.com/services/education/admin/
      description: This course covers installation, configuration and maintenance tasks for a GitLab self-managed instance. This course is not intended for gitlab.com (SaaS) customers.
      live_date: "2020-02-01"
      maintainer: "Education Services"
      level: "Advanced"
      use_case:
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "Get started with GitLab tutorials"
      url: https://docs.gitlab.com/ee/tutorials/
      description: |
        Get started with GitLab tutorials is a handpicked set of tutorials and resources to help you learn how to use GitLab. It includes topics on Finding your way around GitLab, Use Git, Plan your work in projects, Use CI/CD pipelines, and Secure your application.
      live_date: "2022-01-13"
      maintainer: "Technical Writing"
      level: "Beginner"
      use_case: "GitLab Basics"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "DevOps with GitLab CI Course - Build Pipelines and Deploy to AWS"
      url: https://levelup.gitlab.com/learn/course/devops-with-gitlab-ci-course-build-pipelines-and-deploy-to-aws
      description: |
        This FreeCodeCamp course will teach you how to use GitLab CI to create CI/CD pipelines for building and deploying software to AWS. Original author is Valentin Despa, GitLab hero, who collaborated and reviewed together with Michael Friedrich on the Developer Evcangelism team.
      live_date:
      maintainer: "Developer Evangelism"
      level: "Beginner"
      use_case: "DevOps"
      pace: "Self-Paced"
      assessment: "None"
      registration: "None"
      confidentiality: "Public"

    - name: "Backwards Compatibility Training"
      url: https://levelup.gitlab.com/learn/course/backwards-compatibility-training
      description: With multi-node setups, at any given time two versions of the application can run, the code needs to be backward compatible in order to support zero-downtime upgrades. Learn about the common causes of compatibility problems during upgrades and best practices to prevent them.
      live_date:
      maintainer: "Engineering"
      level: "Beginner"
      use_case: "Version Control & Collaboration"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "Technical Writing Fundamentals"
      url: https://levelup.gitlab.com/learn/course/gitlab-technical-writing-fundamentals
      description: |
        The GitLab Technical Writing Fundamentals course is available to help both GitLab and community contributors write and edit documentation for the product. This course provides direction on grammar, writing, and topic design.
      live_date: "2021-08-02"
      maintainer: "Engineering"
      level: "Beginner"
      use_case: "Talent & Development"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"


