---
  title: 継続的なソフトウェアコンプライアンス
  description: コンプライアンスを組み込んで、リスクを軽減し、効率を高めましょう。
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: 継続的なソフトウェアコンプライアンス
        subtitle: コンプライアンスを組み込んで、リスクを軽減し、効率を高めましょう。
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: 無料トライアルを開始
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: ご質問はこちらから
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像: 継続的なソフトウェアコンプライアンスを実現するGitLab"
    - name: 'by-solution-intro'
      data:
        text:
          highlight: ソフトウェアのコンプライアンスは、チェック項目を選択するだけではありません。
          description: クラウドネイティブアプリケーションは、コンテナ、オーケストレーター、Web API、その他のInfrastructure as Code (IaC)を介した、まったく新しいアタックサーフェスとなっています。複雑なDevOpsツールチェーンとともに、こうした新たなアタックサーフェスが生じたことで、悪名高いソフトウェアサプライチェーン攻撃を受け、結果的に新たな規制要件が定められました。継続的なソフトウェアコンプライアンスは、単にコード自体のセキュリティ上の欠陥を減らすだけでなく、クラウドネイティブアプリケーションとDevOpsの自動化での固有のリスクを管理する重要な方法になりつつあります。
    - name: 'by-solution-benefits'
      data:
        title: Compliance. Security. Simplified.
        image:
          image_url: "/nuxt-images/resources/resources_11.jpeg"
          alt: 公共部門向けGitLab
        is_accordion: true
        items:
          - header: 安心してコード変更を確定
            icon:
              name: release
              alt: チェックマーク付きの盾アイコン
              variant: marketing
            text: ライセンスのコンプライアンスとセキュリティスキャンは、コード変更を確定するたびに自動的に実行されます。
          - header: 確実にポリシーを実施できるようにするコンプライアンスパイプライン
            icon:
              name: pipeline-alt
              alt: パイプラインのアイコン
              variant: marketing
            text: 標準的な規制管理、それとも独自のポリシーフレームワークであるかどうかにかかわらず、重要なポリシーが確実に適用されるようにします。
          - header: 監査を簡素化
            icon:
              name: magnifying-glass-code
              alt: コードを虫眼鏡で拡大するアイコン
              variant: marketing
            text: 自動化されたポリシーは、可視性とトレーサビリティにより監査を簡素化します。
          - header: すべてを1か所で管理
            icon:
              name: devsecops
              alt: DevSecOpsアイコン
              variant: marketing
            text: GitLabでは、管理するアプリケーションは1つだけです。サイロ化は発生せず、システム間のやり取り時に情報を損失することもありません。
    - name: 'by-solution-value-prop'
      data:
        title: コンプライアンスを考慮した単一のDevOpsプラットフォーム
        cards:
          - title: ビルトインコントロール
            description: ソフトウェアのコンプライアンスは、ソフトウェア開発プロセスから切り離されると困難になることがあります。組織は、既存のワークフローやプロセスに追加されたものではなく、組み込まれたコンプライアンスプログラムを必要としています。<a href="https://page.gitlab.com/resources-ebook-modernsoftwarefactory.html" data-ga-name="Guide to Software Supply Chain Security" data-ga-location="body">ソフトウェアサプライチェーンのセキュリティに関するガイド</a>をダウンロードして、詳細をご覧ください。
            icon:
              name: visibility
              alt: 可視化アイコン
              variant: marketing
          - title: ポリシーの自動化
            description: コンプライアンスを制御すると、コンプライアンス違反やプロジェクトの遅延といったリスクを軽減しながら迅速なソフトウェア開発が可能になります。すべてを1か所にまとめることで、監査を簡素化します。
            icon:
              name: continuous-integration
              alt: 継続的インテグレーションアイコン
              variant: marketing
          - title: コンプライアンスをシフトレフト
            description: 早い段階でセキュリティの欠陥を見つけて修正したいと考えるように、コンプライアンスの欠陥でも早期に対応することが最も効率的です。コンプライアンスを開発ライフサイクルに埋め込めれば、コンプライアンスもシフトレフトできます。
            icon:
              name: less-risk
              alt: 低リスクアイコン
              variant: marketing
          - title: コンプライアンスのフレームワーク
            description: ラベルの付いたプロジェクトに、共通のコンプライアンス設定を簡単に適用できます。
            list:
              - text: <a href="https://about.gitlab.com/solutions/pci-compliance/" data-ga-name="PCI Compliance" data-ga-location="body" >PCIコンプライアンス</a>
              - text: <a href="https://about.gitlab.com/solutions/hipaa-compliance/" data-ga-name="HIPAA Compliance" data-ga-location="body" >HIPAAコンプライアンス</a>
              - text: <a href="https://about.gitlab.com/solutions/financial-services-regulatory-compliance/" data-ga-name="Financial Services Regulatory Compliance" data-ga-location="body" >金融サービス規制のコンプライアンス</a>
              - text: <a href="/privacy/privacy-compliance/" data-ga-name="GDPR" data-ga-location="body" >GDPR</a>
              - text: <a href="https://about.gitlab.com/solutions/iec-62304/" data-ga-name="IEC 62304:2006" data-ga-location="body" >IEC 62304:2006</a>
              - text: <a href="https://about.gitlab.com/solutions/iso-13485/" data-ga-name="ISO 13485:2016" data-ga-location="body" >ISO 13485:2016</a>
              - text: <a href="https://about.gitlab.com/solutions/iso-26262/" data-ga-name="ISO 26262-6:2018" data-ga-location="body" >ISO 26262-6:2018</a>
              - text: 独自ポリシーのフレームワーク
            icon:
              name: web-alt
              alt: ウェブのアイコン
              variant: marketing
    - name: 'by-industry-solutions-block'
      data:
        subtitle: 継続的なソフトウェアコンプライアンスを簡素化
        sub_description: "GitLabのコンプライアンス管理機能は、ユーザーがコンプライアンスに準拠したポリシーとフレームワークを定義、適用、およびレポートできるので、シンプルで使いやすく、問題が発生しないようにすることを目指しています。"
        white_bg: true
        sub_image: /nuxt-images/solutions/showcase.png
        solutions:
          - title: ポリシー管理
            description: コンプライアンスのフレームワークと一般的な制御を遵守するためのルールとポリシーを定義します。
            icon:
              name: clipboard-checks
              alt: クリップボードチェックのアイコン
              variant: marketing
            link_text: 詳しく見る
            link_url: https://docs.gitlab.com/ee/administration/compliance.html
            data_ga_name: common controls
            data_ga_location: solutions block
          - title: コンプライアンスに準拠したワークフローの自動化
            icon:
              name: automated-code
              alt: 自動コードアイコン
              variant: marketing
            description:  コンプライアンスの自動化は、定義されたルールとポリシー、および職務分掌を実施しながら、全体的なビジネスリスクを軽減できます。
            link_text: 詳細はこちら
            link_url: https://docs.gitlab.com/ee/administration/compliance.html#compliant-workflow-automation
            data_ga_name: Compliant Workflow Automation
            data_ga_location: solutions block
          - title: 監査管理
            icon:
              name: magnifying-glass-code
              alt: コードを虫眼鏡で拡大するアイコン
              variant: marketing
            description: DevOpsの自動化全体におけるアクティビティをすべて記録して、インシデントを特定し、コンプライアンスルールと定義されたポリシーの遵守を証明します。単一のプラットフォームで可視性が向上するとともに、ツールチェーンのサイロ化を回避できます。
            link_text: 詳細はこちら
            link_url: https://docs.gitlab.com/ee/administration/compliance.html#audit-management
            data_ga_name: Audit management
            data_ga_location: solutions block
          - title: セキュリティテストと脆弱性管理
            description: すべてのコード変更に対してセキュリティスキャンを実施して、ライセンスのコンプライアンスを確保し、DevOpsエンジニアやセキュリティ担当者が脆弱性を追跡および管理できるようにします。
            icon:
              name: approve-dismiss
              alt: 承認および無視アイコン
              variant: marketing
            link_text: 詳しく見る
            link_url: https://about.gitlab.com/solutions/security-compliance/
            data_ga_name: Security testing and vulnerability management
            data_ga_location: solutions block
          - title: ソフトウェアサプライチェーンのセキュリティ
            description: 従来のアプリケーションセキュリティテストだけでなく、クラウドネイティブアプリケーションやDevOps自動化のアタックサーフェスをエンドツーエンドで管理します。
            link_text: 詳細はこちら
            icon:
              name: release
              alt: チェックマーク付きの盾アイコン
              variant: marketing
            link_url: https://about.gitlab.com/direction/supply-chain/
            data_ga_name: offline environment
            data_ga_location: solutions block
    - name: 'solutions-resource-cards'
      data:
        column_size: 4
        cards:
          - icon:
              name: video
              alt: 動画アイコン
              variant: marketing
            event_type: "動画"
            header: "コンプライアンスパイプライン"
            link_text: "今すぐ視聴"
            image: "/nuxt-images/solutions/compliance/compliant-pipelines.jpeg"
            href: "https://www.youtube.com/embed/jKA_e_jimoI"
            data_ga_name: "Compliant pipelines"
            data_ga_location: resource cards
          - icon:
              name: blog
              alt: ブログアイコン
              variant: marketing
            event_type: "ブログ"
            header: "GitLabのセキュリティのあまり知られていない3つのこと"
            link_text: "詳細はこちら"
            href: "https://about.gitlab.com/blog/2021/11/23/three-things-you-might-not-know-about-gitlab-security/"
            image: "/nuxt-images/resources/resources_1.jpeg"
            data_ga_name: "Three things you may not know about GitLab security"
            data_ga_location: resource cards
          - icon:
              name: webcast
              alt: ウェブキャストアイコン
              variant: marketing
            event_type: "ウェビナー"
            header: "ソフトウェアサプライチェーンを保護する"
            link_text: "詳細はこちら"
            href: "https://www.youtube.com/watch?v=dZfS4Wt5ZRE"
            fallback_image: "/nuxt-images/resources/fallback/img-fallback-cards-gitops.png"
            data_ga_name: "Secure your software supply chain"
            data_ga_location: resource cards
          - icon:
              name: ebook
              alt: 電子書籍のアイコン
              variant: marketing
            event_type: "電子書籍"
            header: "ソフトウェアサプライチェーンのセキュリティ"
            link_text: "詳細はこちら"
            href: "https://learn.gitlab.com/devsecops-aware/software-supply-chain-security-ebook"
            fallback_image: "/nuxt-images/resources/fallback/img-fallback-cards-gitlab.png"
            data_ga_name: "Software Supply Chain Security"
            data_ga_location: resource cards
          - icon:
              name: case-study
              alt: 事例アイコン
              variant: marketing
            event_type: "お客様事例"
            header: "Chorus社の事例"
            link_text: "詳細はこちら"
            image: "/nuxt-images/resources/resources_2.jpeg"
            href: "/customers/chorus/"
            data_ga_name: "Chorus case study"
            data_ga_location: resource cards