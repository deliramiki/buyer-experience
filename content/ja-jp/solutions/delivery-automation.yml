---
  title: GitLabによるソフトウェアデリバリーの自動化
  description: ソフトウェアの自動化によって生産性とイノベーションを加速しましょう
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        title: ソフトウェアデリバリーの自動化
        subtitle: 速度の向上、デジタルイノベーション、クラウドネイティブトランスフォーメーション、アプリケーションのモダナイゼーションを達成するためには自動化が不可欠
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: 無料トライアルを開始
          url: /free-trial/
        secondary_btn:
          text: ご質問はこちら
          url: /sales/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像：ソフトウェアデリバリーの自動化のためのGitLab"
    - name: 'side-navigation-variant'
      links:
        - title: 概要
          href: '#overview'
        - title: 機能
          href: '#capabilities'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-intro'
              data:
                text:
                  highlight: GitLabによってソフトウェアデリバリーを自動化することで、
                  description: チームの生産性および運用効率が高まり、アプリケーションのモダナイゼーションが加速され、デジタルトランスフォーメーションの価値が発揮されるため、ソフトウェアの開発を高速化できます。
            - name: 'by-solution-benefits'
              data:
                title: ソフトウェアデリバリーの自動化が必要な理由
                video:
                  video_url: "https://player.vimeo.com/video/725654155"
                is_accordion: false
                items:
                  - icon:
                      name: increase
                      alt: 増加アイコン
                      variant: marketing
                    header: クラウドネイティブのアドプションやアプリのモダナイゼーションを高速化
                    text: ClickOpsを廃止して、チェックアンドバランスのアプローチを採用して、高品質のアプリケーションを大規模に構築します。
                  - icon:
                      name: gitlab-release
                      alt: GitLabリリースアイコン
                      variant: marketing
                    header: 高品質で安全なソフトウェアをより迅速に出荷
                    text: アプリケーションリリースプロセスを自動化して、オンデマンドのソフトウェアデリバリーを繰り返し実現できるようにします。
                  - icon:
                      name: collaboration
                      alt: 共同作業アイコン
                      variant: marketing
                    header: 開発者の生産性と作業ワークフローを向上
                    text: マニュアルで繰り返し行うタスクを最小限に抑え、価値を生み出す重要な作業に集中できるようになります。
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-showcase'
              data:
                title: ソフトウェアデリバリープロセスを自動化するには
                description: アプリケーションの品質を向上するには、開発者は付加価値のあるタスクに注力し、コンテキストを切り替えることなく、チーム間で効果的に共同作業を行う必要があります。
                image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
                items:
                  - title: 継続的な変更管理
                    text: ソフトウェアデリバリーチーム全体で調整、共有、共同作業を行えるようにすることで、ビジネス価値をより迅速に生み出します。
                    list:
                      - エンタープライズに対応したソースコード管理
                      - "すべての変更を追跡 - アプリケーションコード、インフラストラクチャ、ポリシー、設定"
                      - すべての変更を管理 - コードオーナー、承認者、ルール
                      - 地理的に分散したチームのための分散バージョン管理
                    link:
                      text: 詳細はこちら
                      href: "/solutions/source-code-management/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: gitlab-cd
                      alt: GitLab CDアイコン
                      variant: marketing
                    video: https://www.youtube.com/embed/JAgIEdYhj00?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
                  - title: 継続的インテグレーションと検証
                    text: 高品質のアプリケーションを大規模に構築することで、デジタルトランスフォーメーションを加速します。
                    list:
                      - すべての変更を段階的にビルドおよびテストできるよう、コーディング、ビルド、テストを自動化
                      - エラーを早期に検出することでリスクを軽減
                      - 並列ビルド、マージトレインによってスケール可能
                      - マルチプロジェクトパイプラインを使用してプロジェクト間でコラボレーション
                    link:
                      text: 詳細はこちら
                      href: "/solutions/continuous-integration/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: continuous-integration
                      alt: 継続的インテグレーションアイコン
                      variant: marketing
                    video: https://www.youtube.com/embed/ljth1Q5oJoo?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
                  - title: GitOpsを使用したオンデマンド環境
                    text: マニュアルによるインフラストラクチャの設定とClickOpsによるリスクを最小限に抑えることで、反復可能で安定した環境を作成します。
                    list:
                      - インフラストラクチャを自動化してリリースを高速化
                      - エラー検出時に迅速に復旧
                      - プッシュまたはプル設定の選択
                      - Kubernetesクラスターへのアクセスを保護し、クラスターの公開を回避
                    link:
                      text: 詳細はこちら
                      href: "/solutions/gitops/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: cog-code
                      alt: 歯車とコードのアイコン
                      variant: marketing
                    video: https://www.youtube.com/embed/onFpj_wvbLM?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
                  - title: 継続的デリバリー
                    text: アプリケーションリリースプロセスを自動化して、オンデマンドのソフトウェアデリバリーを繰り返し実現できるようにします。
                    list:
                      - すべての変更を「リリース可能」に
                      - 中断を最小限に抑えるために、変更を段階的に展開
                      - 一部のユーザーを対象に変更をテストすることで、より迅速にフィードバックを入手
                    link:
                      text: 詳細はこちら
                      href: "/stages-devops-lifecycle/continuous-delivery/"
                      data_ga_name:
                      data_ga_location: body
                    icon:
                      name: continuous-delivery
                      alt: 継続的デリバリーアイコン
                      variant: marketing
                    video: https://www.youtube.com/embed/L0OFbZXs99U?enablesjsapi=1&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: group-buttons
      data:
        header:
          text: デリバリーの自動化に役立つGitLabのその他のソリューション
          link:
            text: その他のソリューションを見る
            href: /solutions/
        buttons:
          - text: 継続的インテグレーション
            icon_left: continuous-delivery
            href: /solutions/continuous-integration/
          - text: 継続的なソフトウェアセキュリティ
            icon_left: devsecops
            href: /solutions/continuous-software-security-assurance/
          - text: ソースコード管理
            icon_left: cog-code
            href: /solutions/source-code-management/
    - name: 'report-cta'
      data:
        layout: "dark"
        title: アナリストレポート
        reports:
        - description: "GitLabが『Forrester Wave™: 2023年度第二四半期統合ソフトウェア提供プラットフォーム』において唯一のリーダーに認定"
          url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
        - description: "GitLabが『2023 Gartner® Magic Quadrant™ for DevOps Platforms』においてリーダーの1社に位置付け"
          url: /gartner-magic-quadrant/
    - name: 'solutions-resource-cards'
      data:
        cards:
          - icon:
              name: ebook
              alt: eBookのアイコン
              variant: marketing
            event_type: "ソリューションカタログ"
            header: "ソフトウェアデリバリーの自動化ガイド"
            link_text: "詳細はこちら"
            fallback_image: /nuxt-images/blogimages/nvidia.jpg
            href: "https://learn.gitlab.com/automated-software-delivery/solution-brief-asd?lb_email={{lead.email address}}&utm_medium=other&utm_campaign=autosd&utm_content=autosdpage"
          - icon:
              name: ebook
              alt: eBookのアイコン
              variant: marketing
            event_type: "eBook"
            header: "ソフトウェアデリバリーの自動化によるROIを測定"
            link_text: "ROIに関する考慮事項について読む"
            fallback_image: /nuxt-images/blogimages/worldline-case-study-image.jpg
            href: "https://page.gitlab.com/autosd-roi.html"
          - icon:
              name: ebook
              alt: eBookのアイコン
              variant: marketing
            event_type: "eBook"
            header: "CI/CDのモダナイゼーション"
            link_text: "詳細はこちら"
            fallback_image: /nuxt-images/blogimages/modernize-cicd.jpg
            href: "/resources/ebook-fuel-growth-cicd/"
            data_ga_name: "Modernize your CI/CD"
            data_ga_location: "body"
          - icon:
              use_local_icon: true
              name: /nuxt-images/logos/gitlab.svg
              alt: GitLabアイコン
            event_type: "計算ツール"
            header: "ツールチェーンにどれだけコストをかけていますか？"
            link_text: "ROIを計算"
            href: "https://about.gitlab.com/calculator/roi"
            fallback_image: /nuxt-images/resources/resources_3.jpg
            data_ga_name: "Calculate ROI"
            data_ga_location: "body"
          - icon:
              name: ebook
              alt: eBookのアイコン
              variant: marketing
            event_type: "eBook"
            header: "GitOps初心者向けガイド"
            link_text: "詳細はこちら"
            href: "https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html"
            fallback_image: /nuxt-images/blogimages/blog-performance-metrics.jpg
          - icon:
              name: ebook
              alt: eBookのアイコン
              variant: marketing
            event_type: "eBook"
            header: "今すぐGitOpsを導入すべき10の理由"
            link_text: "詳細はこちら"
            href: "https://page.gitlab.com/gitops-enterprise-ebook.html"
            fallback_image: /nuxt-images/blogimages/gitops-image-unsplash.jpg
          - icon:
              name: ebook
              alt: eBookのアイコン
              variant: marketing
            event_type: "eBook"
            header: "CI/CDのスケーリング"
            link_text: "詳細はこちら"
            href: "/resources/scaled-ci-cd/"
            fallback_image: /nuxt-images/blogimages/modernize-cicd.jpg
            data_ga_name: "Scaled CI/CD"
            data_ga_location: "body"
          - icon:
              name: case-study
              alt: 事例アイコン
              variant: marketing
            event_type: "事例"
            header: "GitOpsの成功"
            link_text: "顧客3社から学ぶ"
            href: "https://learn.gitlab.com/gitops-awereness-man-1/gitops_automation_success"
            fallback_image: /nuxt-images/blogimages/xcite_cover_image.jpg
            data_ga_name: "Scaled CI/CD"
            data_ga_location: "body"
