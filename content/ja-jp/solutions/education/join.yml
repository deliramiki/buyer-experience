---
  title: 教育団体向けGitLabプログラムに参加する
  description: 教育団体向けGitLabに参加すれば、お近くの教室にDevOpsを導入できます。今すぐ申請してDevOpsをはじめましょう！
  components:
    - name: 'solutions-hero'
      data:
        title: 教育団体向けGitLab
        subtitle: キャンパスへのDevOpsの導入
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          url: '#application'
          text: 教育団体向けGitLabプログラムに参加する
          data_ga_name: join education program
          data_ga_location: header
          icon:
            name: arrow-down
            variant: product
        image:
          image_url: /nuxt-images/solutions/education.jpeg
          alt: "画像：教育団体向けGitLab"
          rounded: true
    - name: tabs-menu
      data:
        column_size: 8
        menus:
          - id_tag: requirements
            text: 要件
            data_ga_name: requirements
            data_ga_location: header
          - id_tag: application
            text: お申し込み
            data_ga_name: application
            data_ga_location: header
          - id_tag: renewal
            text: 更新
            data_ga_name: renewal
            data_ga_location: header
          - id_tag: frequently-asked-questions
            text: よくある質問
            data_ga_name: frequently asked questions
            data_ga_location: header
    - name: copy-media
      data:
        block:
          - header: 要件
            aos_animation: fade-up
            aos_duration: 500
            hide_horizontal_rule: true
            metadata:
              id_tag: requirements
            id: requirements
            miscellaneous: |
              ##### 教育団体向けGitLabプログラムに参加する教育機関は、次の要件を満たす必要があります。


              * **認定：**教育機関は、地方、州および都道府県、連邦、または国の認定機関によって認定されている必要があります。[詳細はこちら](/handbook/marketing/community-relations/community-programs/education-program/#gitlab-for-education-program-requirements){data-ga-name="accredited" data-ga-location="body"}。
              * **主な教育の目的：**在籍している学生を教育することを主な目的とする教育機関である必要があります。
              * **学位授与：**準学士、学士、および大学院の学位などの学位を積極的に授与する教育機関でなければなりません。
              * **非営利：**非営利の教育機関である必要があります。営利団体は対象外です。

              ##### 教育団体向けGitLabプログラムのライセンスは、次の目的でのみ使用できます。

              * **教育目的での使用：**教育機関の教育機能の一部である学術教育を含む、学生の学習、トレーニング、もしくは育成に直接関連する活動。
              * **非営利の学術研究：**収益を生み出すために誰かが商業的に使用するための結果、作品、サービス、またはデータを生み出さない非営利の研究プロジェクトの実施。第三者の要求に応じて第三者の利益のために行われる研究は、教育団体向けGitLabライセンスの下では許可されません。
              * **教育団体向けGitLabライセンスを使用して機関を経営、管理、または運営することは許可されていません。** GitLabは、キャンパス全体で使用できるアカデミック割引と特別価格を提供しています。[詳細はこちら](/solutions/education/campus/){data-ga-name= "campus pricing" data-ga-location="body"}。

              * **注：**現時点では、13歳未満の学生が在籍する教育機関は、GitLab SaaSの対象外です。これらの機関は、GitLab Self-Managedライセンスを取得することができます。

              ##### 対象となる申請者

              * **教員またはスタッフ：**教育機関でフルタイムで雇用されている教員またはスタッフのみ申請できます。本プログラムでは、学生に対して直接ライセンスを発行することはできません。
              * **メールドメイン：**申請者は、代表機関のメールドメインで申請する必要があります。個人のメールドメインは受け入れられません。

              ##### 国に関する制限事項

              *GitLab, Inc.は、中国にある教育機関にはライセンスを発行しません。中国での教育用のライセンスの取得についての詳細は、[JiHuにお問い合わせください](mailto: ychen@gitlab.cn)。[JiHuの詳細はこちら](/blog/2021/03/18/gitlab-licensed-technology-to-new-independent-chinese-company/){data-ga-name="more about JiHu" data-ga-location="body"}。

              #### 教育団体向けGitLabプログラム契約

              *承認されると、すべてのプログラムメンバーは[教育団体向けGitLabプログラム契約](/handbook/legal/education-agreement/){data-ga-name="education agreement" data-ga-location="body"}の対象となります。

              #### プログラムの特典

              *トップレベルの機能（SaaSまたはSelf-Managed）のライセンスごとのシート数は無制限です。シート数は、翌年中にこのライセンスを使用するユーザー数です。
              *シート数とライセンスの種類（SaaSまたはSelf-Managed）は、更新時またはリクエストすることで変更できます。
              *GitLabのサポートは、教育用のライセンスには含まれていません。
              *サブスクリプションには、CI Runner向けに50,000コンピューティング時間（分）が含まれています([追加の分数は購入する必要があります](https://docs.gitlab.com/ee/subscriptions/#purchasing-additional-ci-minutes){data-ga-name="additional minutes" data-ga-location="body"})。
    - name: copy-form
      data:
        form:
          external_form:
            url: https://offers.sheerid.com/gitlab/university/teacher/
            width: 800
            height: 1300
        header: お申し込み
        metadata:
          id_tag: application
        form_header: お申し込みフォーム
        datalayer: sales

        content: |
          ## お申し込み手続き
          *右側のお申し込みフォームに記入してください。できるだけ正確かつ完全な情報を入力してください。
          *GitLabは、信頼できるパートナーであるSheerIDを使用して、要件を満たす教育機関に在籍する教師、教員、またはスタッフであることを確認します。

          ## お申し込み後の流れ
          お申し込みフォームへの記入後、認証されると、ライセンスを取得するための手順が記載された確認メールが送付されます。注意して指示に従ってください。

          ## ヘルプとサポート
          カスタマーポータルでライセンスを取得する際に問題が発生した場合は、[GitLabサポートポータル]( https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293)でサポートチケットを作成し、[ライセンスと更新の問題]を選択してください。
    - name: copy
      data:
        aos_animation: fade-up
        aos_duration: 500
        block:
          - header: 更新
            metadata:
              id_tag: renewal
            id: renewal
            subtitle: 更新の申請
            column_size: 10
            text: |
              教育団体向けGitLabライセンスは毎年更新する必要があります。プログラムの要件は随時変更される可能性があります。また、続けてご利用いただくメンバーが引き続き要件を満たしていることをご確認いただく必要があります。

              更新を申請する前に：

              *権限を確認してください。サブスクリプションの更新を申請する方は、その機関のGitLabカスタマーポータルでサブスクリプションを作成した方と同じ人物でなければなりません。
              *別のユーザーが更新をリクエストする場合は、既存のオーナーが[カスタマーポータルアカウントの所有権を移管]( https://docs.gitlab.com/ee/subscriptions/#change-account-owner-information)する必要があります。既存のオーナーが所有権を譲渡または更新できなくなった場合は、[サポートチケットを作成して](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000071293)、サブスクリプションのオーナーを変更してください。


              初めてプログラムに申し込む場合でも、既存のメンバーシップを更新する場合でも、使用するフォームは同じです。

              お申し込みフォームへの記入後、認証されると、ライセンスを取得するための手順が記載された確認メールが送付されます。注意深く指示に従ってください。
             
             
    - name: faq
      data:
        metadata:
          id_tag: frequently-asked-questions
        aos_animation: fade-up
        aos_duration: 500
        header: よくある質問
        groups:
          -
            questions:
              - question: 研究は教育団体向けGitLabライセンスの対象となりますか？
                answer: |
                  はい。教育機関が要件を満たしており、非商業的な学術研究である場合は、研究も対象となります。詳細については、本プログラムの要件をご覧ください。
              - question: 同じライセンスキーで複数のSelf-Managedインスタンスを実行できますか？
                answer: |
                  はい。同じライセンスキーを使用して複数のSelf-Managedインスタンスを有効化できます。
              - question: GitLabはなぜ学生に無料ライセンスを直接提供しないのですか？
                answer: 教育団体向けGitLabライセンスプログラムは、個人ではなく機関（エンタープライズレベル）に直接発行することを意図して提供されるプログラムです。対象の機関に所属するすべての学生の方々が、GitLabが提供する最高のサービスにアクセスできるよう、無制限のシート数とトップレベルのライセンスを提供しています。個々の学生がGitLabライセンスを希望する可能性があることは理解していますが、現時点では、個人にライセンスを付与するためのフローはありません。学生の方々には、教員またはスタッフのスポンサーを見つけて、プログラムにお申し込みいただくことをおすすめします。また、GitLab.comの無料サブスクリプションプランや、ダウンロードできるSelf-ManagedのFreeプランもぜひご確認ください。なお、さらに高度な機能を試したい場合は、30日間の試用版にお申し込みいただけます。
              - question: プロジェクトの表示レベルをどのように管理すればよいですか？
                answer: |
                  GitLabの親グループのメンバーである場合、すべての子グループに自動的にアクセスできます。GitLabは、親グループよりも制限が厳しいサブグループはサポートしていません。ただし、サブグループのメンバーであっても、親グループへのアクセスが許可されるわけではありません。
                  \
                  最もよい方法は、全員をそれぞれのサブグループのメンバーにし、トップレベルのグループには管理者のみを配置することです。
              - question: このプログラムのライセンスは、IT部門でITサービスを実行するために使用できますか？
                answer: |
                  いいえ。IT担当者による使用、または機関自体を運営するための管理上の使用は対象外です。教育団体向けGitLabライセンスは、教育または研究目的でのみご利用いただけます。IT担当者の方でGitLabを使用することに興味がある場合は、当社のセールスチームにお問い合わせください。
              - question: 学生は卒業後もGitLabインスタンスを使用できますか？
                answer: |
                  教育団体向けGitLabライセンスは、教育機関に直接発行されます。したがって、学生は所属の機関からアクセスが提供されなくなった場合、自分でライセンスを購入する必要があります。
              - question: エンドユーザーライセンス契約への変更は許可されていますか？
                answer: |
                  現時点では、ユーザー契約の変更に対応することはできません。ご不明な点がございましたら、メールでeducation@gitlab.comまでお問い合わせください。
              - question: SSLでLDAPを介してユーザー認証を行えますか？
                answer: |
                  GitLab Self-Managedバージョンでのみ可能です。LDAPサーバーにはDNS名を使用できるため、サーバーは厳密には静的IPは必要ではありません。
              - question: 使用開始後にシート数を増やせますか？
                answer: |
                  既存のライセンスのシート数を増やしたい場合は、メールでeducation@gitlab.comまでお問い合わせください。追加シート用ののアドオンの見積もりをご用意します。
              - question: 誰がサブスクリプションにカウントされますか？
                answer: |
                  詳しくは、ライセンスとサブスクリプションに関するよくある質問をご覧ください。
              - question: どうやってサポートを受けることができますか？
                answer: |
                   サポートの受け方の詳細については、[コミュニティプログラムのサポート](https://docs.gitlab.com/ee/subscriptions/#support-for-community-programs)ドキュメントのセクションをご覧ください。なお、教育団体向けGitLabライセンスのサポートを別途購入することはできなくなりましたのでご注意ください。代わりに、対象となる機関には、[キャンパス向けGitLabサブスクリプション](/solutions/education/campus/)をご購入いただけます。