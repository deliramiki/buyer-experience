---
  title: GitLabによるアジャイル計画
  description: スクラム、かんばん、大規模アジャイルフレームワーク(SAFe)などのアジャイルプロセスにおいて、GitLabをアジャイルプロジェクト管理ツールとして使用する方法をご紹介します。
  report_cta:
    layout: "dark"
    title: アナリストレポート
    reports:
    - description: "GitLabが2023年度第2四半期のForrester Wave™: Integrated Software Delivery Platformsにおける唯一のリーダーに認定"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
    - description: "DevOpsプラットフォームの2023年度Gartner® Magic Quadrant™でGitLabがリーダーに認定"
      url: /gartner-magic-quadrant/
  components:
    - name: 'solutions-hero'
      data:
        title: アジャイル計画
        subtitle: アジャイル向けの統合されたサポートで、プロジェクト、プログラム、および製品の計画と管理を行います。
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: 無料トライアルを開始
          url: /free-trial/
        secondary_btn:
          text: 価格設定について
          url: /pricing/
          data_ga_name: Learn about pricing
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "画像: 公共部門向けGitLab"
    - name: 'copy-media'
      data:
        block:
          - header: GitLabを活用したアジャイル計画
            text: |
              開発チームは、スクラム、かんばん、エクストリームプログラミング(XP)のような反復型、インクリメンタル型、リーン型のプロジェクト手法を用いて、価値の提供を加速し続けています。大企業は、大規模アジャイルフレームワーク(SAFe)、Spotify、大規模スクラム(LeSS)など、さまざまなフレームワークを通じて、企業の規模に即したアジャイルを採用してきました。GitLabを使用すれば、チームが選択した開発手法に関係なく、アジャイル手法と原則を適用して作業を整理して管理できます。
            link_href: /demo/
            link_text: 詳細はこちら
            video:
              video_url: https://www.youtube.com/embed/wmtZKC8m2ew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: GitLabのメリット
            text: |
              最も包括的なDevSecOpsプラットフォームであるGitLabは、次のような特徴を備えています。

              *   **シームレス：** GitLabは、単一的なユーザーエクスペリエンスと標準ツールセットによるコラボレーションの支援および可視性の改善を通じて、計画からデプロイまでのステップに加え、その先のプロセスまでにわたり、アジャイルチームを支援します
              *   **一体型：**作業とプロジェクト管理を同じシステム内で行えます
              *   **スケーラビリティ：**複数のアジャイルチームを管理できるため、大企業のアジャイルのスケーラビリティに対応可能です
              *   **柔軟性：**独自のアジャイル手法を実行するにせよ、形式的なフレームワークを適用するにせよ、それぞれの開発手法のニーズに応じて、すぐに使用できる機能をカスタマイズできます
              *   **習得しやすさ：**アジャイルチームの立ち上げに関する[クイックスタートガイド](https://www.youtube.com/watch?v=VR2r1TJCDew){data-ga-name="quick start" data-ga-location="body"}と[チュートリアル]( https://docs.gitlab.com/ee/tutorials/plan_and_track.html)をご覧ください。
            image:
              image_url: /nuxt-images/blogimages/cicd_pipeline_infograph.png
              alt: ""
          - header: アジャイルプロジェクトの管理
            inverted: true
            text: |
              GitLabでは、基本的なイシュー追跡、スクラム、かんばんスタイルのプロジェクト管理など、さまざまな機能を活用した効率的かつアジャイルなプロジェクト管理を行えます。いくつかのイシューを追跡したり、開発者チーム全体でDevSecOpsライフサイクルのプロセスを管理したりする場合など、GitLabはさまざまな状況でチームをサポートします。

              *   [イシュー](https://docs.gitlab.com/ee/user/project/issues/index.html){data-ga-name="issues" data-ga-location="body"}を使用した**計画、割り当て、追跡**
              *   [ラベル](https://docs.gitlab.com/ee/user/project/labels.html){data-ga-name="labels" data-ga-location="body"}、[イテレーション](https://docs.gitlab.com/ee/user/group/iterations/){data-ga-name="iterations" data-ga-location="body"}、[マイルストーン](https://docs.gitlab.com/ee/user/project/milestones/index.html#overview){data-ga-name="milestones" data-ga-location="body"}を使用した**作業の整理**
              *   [ボード](https://docs.gitlab.com/ee/user/project/issue_board.html){data-ga-name="boards" data-ga-location="body"}と[ロードマップ](https://docs.gitlab.com/ee/user/group/roadmap/)を使用した**作業の可視化**
              *   [マージリクエスト](https://docs.gitlab.com/ee/user/project/merge_requests/index.html){data-ga-name="merge requests" data-ga-location="body"}による**作業と成果の直結**
            video:
              video_url: https://www.youtube.com/embed/wmtZKC8m2ew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: プログラムとポートフォリオの管理
            text: |
              可視性を維持し、ビジネスのイニシアチブに応じてメンバーとプロジェクトを管理できます。ポリシーや権限の定義および実施、複数のプロジェクトおよびグループ間の進捗状況と進行速度の追跡、イニシアチブの優先度を設定して、最大限の価値を提供します。

              *   新しいビジネスイニシアチブや工数を[エピック](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="epics" data-ga-location="body"}に取り込んで**整理**できます
              *   サブグループを使用して、セキュリティや可視性を損なうことなく、プログラムにプロジェクトや**チームを連携**できます
              *   [サブエピック](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#multi-level-child-epics){data-ga-name="sub-epics" data-ga-location="body"}とイシューを**計画**して、イテレーションとマイルストーンに組み込むことができます
              *   ロードマップ、[インサイト](https://docs.gitlab.com/ee/user/project/insights/){data-ga-name="insights" data-ga-location="body"}、[バリューストリーム分析](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html){data-ga-name="value stream analytics" data-ga-location="body"}を使用して、価値提供を**可視化**できます
            video:
              video_url: https://www.youtube.com/embed/VR2r1TJCDew?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
          - header: GitLabによる大規模アジャイルフレームワーク(SAFe)
            inverted: true
            text: |
              組織が大規模アジャイルフレームワーク(SAFe)を用いてフレームワークをビルドする際に、GitLabをどのように活用できるかをご覧ください。チーム、プログラム、ポートフォリオの3つの柱をベースとした、開発チーム向けのアジャイルフレームワークの構築について詳しくご説明します。
            video:
              video_url: https://www.youtube.com/embed/PmFFlTH2DQk?enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
    - name: copy-media
      data:
        block:
          - header: 機能
            miscellaneous: |
              *   **イシュー：**まずは、ユーザーにビジネス価値を提供する単一の機能をとらえる[イシュー](https://docs.gitlab.com/ee/user/project/issues/){data-ga-name="start with an issue" data-ga-location="body"}から開始しましょう。
              * **タスク：**ユーザー事例の多くは、個々のタスクに分解できます。GitLabのイシュー内に[タスク](https://docs.gitlab.com/ee/user/tasks.html){data-ga-name="task" data-ga-location="body"} を作成して、個々の作業を細かく区別しましょう。
              *   **エピック：**プロジェクトやマイルストーン全体で、共通のテーマを持ったイシューグループを[エピック](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="milestones with epics" data-ga-location="body"}で追跡することで、プロジェクトのポートフォリオ管理の手間が減り、効率化できます。
              * **イテレーション：**[イテレーション](https://docs.gitlab.com/ee/user/group/iterations/){data-ga-name="iterations" data-ga-location="body"}を使用して、チームのスプリントにイシューを割り当てるケイデンスを作成しましょう。
              *   **イシューボード：**すべてを1か所で管理できます。製品を切り替えることなく、イシューを[追跡](https://docs.gitlab.com/ee/user/project/issues/){data-ga-name="track" data-ga-location="body"}したり、進捗を共有し合ったりできます。バックログから完了まで、1つのインターフェイスでイシューの状況を把握できます。
              *   **ロードマップ：**開始日や期限をタイムライン形式で可視化できます。エピックの[ロードマップ](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="roadmap" data-ga-location="body"}ページには、グループやそのサブグループに含まれるすべてのエピックの情報がタイムラン形式で表示されます。
              *   **バーンダウンチャート：**作業をリアルタイムで追跡し、リスクが発生した時点でリスクを軽減できます。[バーンダウン](https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html#burndown-charts){data-ga-name="burndown" data-ga-location="body"}チャートを使用すれば、チームは現在のスプリントのスコープとなっている作業を、その完了の度に可視化していくことが可能です。
              *   **共同作業：** GitLabなら、イシュー、エピック、マージリクエスト、コミットなど、さまざまな場面において会話形式で[貢献](https://docs.gitlab.com/ee/user/discussions/){data-ga-name="contribute" data-ga-location="body"}できます!
              *   **トレーサビリティ：**チームのイシューを後続の[マージリクエスト](https://docs.gitlab.com/ee/user/project/merge_requests/){data-ga-name="merge requests" data-ga-location="body"}と連携させることで、関連したパイプラインが通った時点で、イシューの作成から終了時までの完全なトレーサビリティを得られます。
              *   **Wiki：**コードとドキュメントを同じプロジェクト内に保管しておきたい場合、[Wiki](https://docs.gitlab.com/ee/user/project/wiki/){data-ga-name="wiki" data-ga-location="body"}と呼ばれるドキュメントシステムを使用できます。
              *   **エンタープライズアジャイルフレームワーク：**大企業は、さまざまなフレームワークを使用し、企業規模に合わせてアジャイルをスケールしています。GitLabなら、[SAFe](https://www.scaledagileframework.com/)、Spotify、ディシプリンドアジャイルデリバリーなどをサポートできます。
    - name: 'copy-media'
      data:
        block:
          - header: GitLabによるアジャイルイテレーション
            subtitle: ユーザーストーリー → GitLabイシュー
            text: |
              アジャイル手法では、多くの場合、ユーザーにビジネス価値を提供する1つの機能を示したユーザー事例からはじまります。GitLabでは、プロジェクト内の1つのイシューがこの役割を果たします。
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-list.png
              alt: ""
          - subtitle: タスク → GitLabタスク
            inverted: true
            text: |
              ほとんどのユーザー事例は、個々のタスクに分解できます。GitLabのイシュー内にタスクを作成することで、個々の作業単位を細かく区別できます。
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Tasks.png
              alt: ""
          - subtitle: エピック → GitLabエピック
            text: |
              アジャイルの専門家の中には、複数の機能で構成されるより大きなユーザーフローを示す、エピックとも呼ばれる抽象化したものを、ユーザー事例の上に指定する人もいます。GitLabでは、エピックを使用すると、作業を複数の子エピックやイシューに分解して、多くのグループやプロジェクトにまたがる複雑なイニシアチブを表すことができます。
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue.png
              alt: ""
          - subtitle: プロダクトバックログ → GitLabのイシューリストと優先ラベル
            inverted: true
            text: |
              多くの場合、プロダクトオーナーやビジネスオーナーは、ビジネスや顧客のニーズを反映するためにこれらのユーザー事例を作成します。GitLabでは、バックログを追跡することを目的にユーザーが表示できるイシューリストが、動的に生成されます。ラベルを使用すると、チームにとって重要なことを反映するように、ビューを柔軟にフィルタリングおよびカスタマイズできます。
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue-board.png
              alt: ""
          - subtitle: スプリント → GitLabのイテレーション
            text: |
              スプリントとは、作業を完了させるまでの期限を設けた期間を指します。1週間、数週間、または1か月以上と、その期間はさまざまです。GitLabイテレーションのケイデンスは、チームのスプリントのケイデンスをとらえるために使用できます。自動化されたイテレーション管理により、管理者の負担を軽減します。
            image:
              image_url: /nuxt-images/solutions/agile-delivery/Milestones.png
              alt: ""
          - subtitle: ポイントと推測 → GitLabのイシューの重さ
            inverted: true
            text: |
              GitLabのイシューにはウェイト属性があり、推測される工数をとらえられますイシューをタスクに分解して、より正確な推定を実行します。タイムトラッキングを使用すると、チームは問題に費やした時間を推定および追跡できます。
            image:
              image_url: /nuxt-images/solutions/agile-delivery/weight.png
              alt: ""
          - subtitle: アジャイルボード → GitLabイシューボード
            text: |
              イシューボードは、コラボレーションの中心的なポイントを提供し、イシューのライフサイクルを定義します。イシューボードでは、GitLabでプロセス全体を可視化し、管理できます。ラベルを使用してリストを作成し、データをフィルタリングしてチームのプロセスのビューを作成します。
            image:
              image_url: /nuxt-images/solutions/agile-delivery/issue_board.png
              alt: ""
          - subtitle: バーンダウンチャート → GitLabバーンダウンチャート
            inverted: true
            text: |
              GitLabのバーンダウンチャートを使用すれば、チームはスプリントのスコープとなっている作業が完了する度に「燃え尽きていく」様子を可視化できます。指定されたイテレーションまたはマイルストーンの完了状況を一目で把握できます。
            image:
              image_url: /nuxt-images/solutions/agile-delivery/burndown-chart.png
              alt: ""
    - name: copy-resources
      data:
        hide_horizontal_rule: true
        block:
          - title: リソース
            subtitle: 次のステップに進む準備はできていますか?
            text: |
              ご質問がありましたら、[GitLabに関するヘルプ](/get-help/){data-ga-name="get help" data-ga-location="body"}を参照してください
            resources:
              video:
                header: 動画
                links:
                  - text: GitLabでアジャイルチームを立ち上げるためのガイドを視聴
                    link: https://youtu.be/VR2r1TJCDew
                  - text: GitLabでSAFe (大規模アジャイルフレームワーク)を実践する方法
                    link: https://youtu.be/PmFFlTH2DQk
                  - text: GitLabのイシューボードの仕組み
                    link: https://youtu.be/CiolDtBIOA0
                  - text: 設定可能なイシューボード
                    link: https://youtu.be/m5UTNCSqaDk
              blog:
                header: ブログ
                links:
                  - text: GitLabをアジャイル向けに使用する方法
                    link: /blog/2018/03/05/gitlab-for-agile-software-development/
                    data_ga_name: GitLab for Agile
                    data_ga_location: body
                  - text: ボードの4つの使い方
                    link: /blog/2018/08/02/4-ways-to-use-gitlab-issue-boards/
                    data_ga_name: 4 ways to use Boards
                    data_ga_location: body