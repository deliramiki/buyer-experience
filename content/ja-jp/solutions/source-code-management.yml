template: feature
title: 'ソースコード管理'
description: 'GitLabのソースコード管理（SCM）を使用すると、開発チームがコラボレーションを通じて生産性を最大限に高めることができ、ソフトウェアデリバリーのスピードアップおよび可視性の向上が促進されます。'
components:
  feature-block-hero:
    data:
      title: ソースコード管理
      subtitle: GitLabでソースコード管理が簡単に
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        text: 無料トライアルを開始
        url: /free-trial/
      image:
        image_url: "/nuxt-images/devops-graphics/create.png"
        hide_in_mobile: true
        alt: ""
  side-navigation:
    links:
      - title: 概要
        href: '#overview'
      - title: メリット
        href: '#benefits'
    data:
      feature-overview:
        data:
          content_summary_bolded: GitLabのバージョン管理
          content_summary: >-
            を使用すると、開発チームがコラボレーションを通じて生産性を最大限に高めることができ、ソフトウェアデリバリーのスピードアップおよび可視性の向上が促進されます。Gitベースのリポジトリを使用するGitLabを使用すれば、わかりやすいコードレビュー、アセットのバージョン管理、フィードバックループ、および強力なブランチパターンを実現できるため、開発者が問題を解決し、顧客に価値を届けやすくなります。
          content_image: /nuxt-images/features/source-code-management/overview.jpg
          content_heading: すべての人のためのバージョン管理
          content_list:
            - クラウドネイティブのアドプションに向けてソフトウェア開発ライフサイクルを拡張
            - Gitベースのリポジトリを使用しているため、開発者がローカルコピーから作業できる
            - コミットごとにコード品質とセキュリティを自動スキャン
            - ビルトインの継続的インテグレーションと継続的デリバリー
      feature-benefit:
        data:
          feature_heading: ソフトウェア開発を変革
          feature_cards:
            - feature_name: 共同作業
              icon:
                name: collaboration
                variant: marketing
                alt: 共同作業アイコン
              feature_description: >-
                デリバリーを高速化... 開発者チームが共同作業を行いながら生産性を最大限に高めることができ、ソフトウェアデリバリーのスピードアップおよび可視性の向上が促進されます。
              feature_list:
                - 'コードのレビュー、コメント、改善'
                - 再利用とインナーソースを実現
                - ファイルロックによって競合を防止
                - 堅牢なWeb IDEによって、あらゆるプラットフォームにおける開発を加速
            - feature_name: 加速
              icon:
                name: increase
                variant: marketing
                alt: 増加アイコン
              feature_description: >-
                常にリリース可能... アセットのバージョン管理、フィードバックループ、および強力なブランチパターンを実現できるため、開発者が問題を解決し、顧客に価値を届けやすくなります。
              feature_list:
                - Gitベースのリポジトリを使用しているため、開発者がローカルコピーから作業できる
                - 'コードのブランチを作成し、変更を加えた後に、メインブランチにマージ'
                - 堅牢なWeb IDEによって、あらゆるプラットフォームにおける開発を加速
            - feature_name: コンプライアンスとセキュリティ
              icon:
                name: release
                variant: marketing
                alt: チェックマーク付きの盾のアイコン
              feature_description: >-
                追跡とトレース... チームは信頼できる唯一の情報源を活用して作業を管理できます。
              feature_list:
                - >-
                  強力なマージリクエストを使用して、コード変更のレビュー、追跡、承認を行える
                - コミットするたびにコード品質とセキュリティを自動スキャン
                - >-
                  きめ細かなアクセス制御およびレポートにより、監査とコンプライアンスを簡素化
  feature-block-related:
    data:
      - 継続的インテグレーション
      - エピックおよびイシューボード
      - セキュリティダッシュボード
      - 承認ルール
      - 脆弱性管理
  group-buttons:
    data:
      header:
        text: ソースコード管理に役立つGitLabのその他のソリューション
        link:
          text: その他のソリューションを確認
          href: /solutions/
      buttons:
        - text: デリバリーの自動化
          icon_left: automated-code
          href: /solutions/delivery-automation/
        - text: 継続的インテグレーション
          icon_left: continuous-delivery
          href: /solutions/continuous-integration/
        - text: 継続的なソフトウェアセキュリティ
          icon_left: devsecops
          href: /solutions/continuous-software-security-assurance/
  report-cta:
    layout: "dark"
    title: アナリストレポート
    reports:
    - description: "GitLabが『Forrester Wave™: 2023年度第二四半期統合ソフトウェア提供プラットフォーム』において唯一のリーダーに認定"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      link_text: レポートを読む
    - description: "GitLabが『2023 Gartner® Magic Quadrant™ for DevOps Platforms』においてリーダーの1社に位置付け"
      url: /gartner-magic-quadrant/
      link_text: レポートを読む
  solutions-resource-cards:
    data:
      title: リソース
      link:
        text: すべてのリソースを表示する
      cards:
        - icon:
            name: webcast
            variant: marketing
            alt: ウェブキャストアイコン
          event_type: ウェブキャスト
          header: 境界線のないコラボレーション - GitLabでより速いデリバリーを実現
          link_text: 詳細はこちら
          image: /nuxt-images/features/resources/resources_webcast.png
          href: /webcast/collaboration-without-boundaries/
          data_ga_name: Collaboration without Boundaries
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: 事例アイコン
          event_type: 事例
          header: >-
            ヴィクトリア大学ウェリントン（テヘレンガワカ校）でGitLabを使用してオープンサイエンス教育を推進
          link_text: 詳細はこちら
          href: /customers/victoria_university/
          image: /nuxt-images/features/resources/resources_case_study.png
          data_ga_name: GitLab advances open science education at Te Herenga Waka
          data_ga_location: body
        - icon:
            name: partners
            variant: marketing
            alt: パートナーアイコン
          event_type: パートナー
          header: GitLab on AWSの利点のご紹介
          link_text: 今すぐ視聴
          href: /partners/technology-partners/aws/
          image: /nuxt-images/features/resources/resources_partners.png
          data_ga_name: Discover the benefits of GitLab on AWS
          data_ga_location: body