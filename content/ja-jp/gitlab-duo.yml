title: GitLab Duo
description: ワークフローを強化する一連のAI機能
video:
  text: GitLab Duoのご紹介
  source: https://player.vimeo.com/video/855805049?badge=0&autopause=0&player_id=0&app_id=58479"
hero: 
  note: GitLab Duo
  description: ワークフローを強化する一連のAI機能
  image: 
    src: /nuxt-images/solutions/ai/GitLab Logo.png
    alt: AIロゴ
  button:
    text: Ultimateの無料トライアルを入手
    href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
    variant: secondary
  banner:
    href: /solutions/code-suggestions/
    text: GitLab Duo Code Suggestions now available
journey:
  features:
    - name: AIを活用したワークフロー
      description: ソフトウェア開発ライフサイクルのすべての段階で、AIの助けを借りて効率を上げ、サイクルタイムを短縮します。
    - name: プライバシー第一、エンタープライズグレード
      description: GitLabでは、企業や規制対象組織がAIを活用したワークフローを採用できるようにするため、プライバシー優先のアプローチを主導しています。
    - name: 単一のアプリケーション
      description: セキュリティが組み込まれた単一のアプリケーションにより、より多くのソフトウェアをより速く提供し、経営陣に対してバリューストリーム全体にわたる可視性を提供し、コンテキストスイッチを回避します。
    - name: すべてのステップですべてのユーザーを支援
      description: 計画およびコード作成からテスト、セキュリティ、そしてモニタリングまで、AIを活用したワークフローによって、開発者、セキュリティ、および運用チームをサポートします。
featureCards:
  title: GitLab Duo機能
  cards: 
    - name: コードの提案
      description: 入力時にコードの提案が表示されるため、より効率的にコードを書くことができます。
      icon: ai-code-suggestions
      href: https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html
    - name: 推奨レビュアー
      description: マージリクエストを確認するのに適した人を自動的に見つけることで、より速く、より高品質のレビューを受け取ることができます。
      icon: ai-reviewer-suggestions
      href: https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#suggested-reviewers
    - name: バリューストリーム予測
      description: 生産性メトリクスを予測し、ソフトウェア開発ライフサイクル全体で異常を特定します。
      icon: ai-value-stream-forecast
      href: https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html
    - name: イシューのコメントを要約
      description: 長いやり取りを素早く確認できるようにし、全員が同じ認識を持てるようにします。
      icon: ai-issue
      href: https://docs.gitlab.com/ee/user/ai_features.html#summarize-issue-discussions
    - name: 提案されたマージリクエストの変更を要約
      description: 変更の影響を効率的に伝えることで、マージリクエストの作成者が調整とアクションを促進させることができます。
      icon: ai-merge-request
      href: https://docs.gitlab.com/ee/user/ai_features.html#summarize-my-merge-request-review
    - name: この脆弱性を説明
      description: 開発者が脆弱性をより効率的に解決し、スキルを向上できるようにすることで、より安全なコードを書けるようにします。
      icon: ai-vulnerability-bug
      href: https://docs.gitlab.com/ee/user/ai_features.html#explain-this-vulnerability-in-the-web-ui
    - name: マージリクエストのレビューを要約
      description: 作成者とレビュアー間のハンドオフを改善し、レビュアーがマージリクエストの提案をより効率的に理解できるようになります。
      icon: ai-summarize-mr-review
      href: https://docs.gitlab.com/ee/user/ai_features.html#summarize-merge-request-changes
    - name: マージリクエストのテスト生成
      description: 反復的なタスクを自動化し、バグを早期に発見できます。
      icon: ai-tests-in-mr
      href: https://docs.gitlab.com/ee/user/ai_features.html#generate-suggested-tests-in-merge-requests
    - name: GitLabチャット
      description: ドキュメントなど、大量のテキスト情報から有用な情報をすばやく特定できます。
      icon: ai-gitlab-chat
      href: https://docs.gitlab.com/ee/user/ai_features.html#gitlab-duo-chat
    - name: このコードを説明
      description: ソースコードの説明を提供して、把握できるように支援します。
      icon: ai-code-explaination
      href: https://docs.gitlab.com/ee/user/ai_features.html#explain-selected-code-in-the-web-ui
principles:
  title: より安全なソフトウェアをより迅速に構築できるようサポート
  slides: 
    - name: デザインによるデータ保護
      description: プライバシー、セキュリティ、およびコンプライアンスを犠牲にして効率を上げることは、誰にとっても効率的ではありません。コードの提案により、独自のソースコードはGitLabのクラウドインフラストラクチャ内で安全に保たれ、トレーニングデータとして使用されることはありません。
      image:
        src: /nuxt-images/solutions/ai/principle-1.png
    - name: 中核となる透明性
      description: 当社は、AIによる支援機能と、お客様のデータを使用および保護する方法について、重要かつ必要な情報を提供します。
      image:
        src: /nuxt-images/solutions/ai/principle-2.png
    - name: 誰でも貢献できる
      description: 当社は、お客様ともに構築できるオープンコアビジネスモデルを採用しています。誰でも、GitLabに新しい機能をご提供いただけます。
      image:
        src:  /nuxt-images/solutions/ai/principle-3.png
resources:
    data:
      title: GitLab AIの新機能
      column_size: 4
      cards:
        - icon:
            name: blog
            variant: marketing
            alt: ブログアイコン
          event_type: ブログ
          header: DevSecOpsシリーズのAI/ML(機械学習)
          link_text: 詳細はこちら
          image: /nuxt-images/blogimages/ai-ml-in-devsecops-blog-series.png
          href: /blog/2023/04/24/ai-ml-in-devsecops-series/
          data_ga_name: AI/ML in DevSecOps Series
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: ブログアイコン
          event_type: ブログ
          header: >-
            GitLab details AI-assisted features in the DevSecOps platform
          link_text: 詳細はこちら
          href: https://about.gitlab.com/blog/2023/05/03/gitlab-ai-assisted-features/
          image: /nuxt-images/blogimages/ai-fireside-chat.png
          data_ga_name: GitLab details AI-assisted features in the DevSecOps platform
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: ブログアイコン
          event_type: ブログ
          header: AIの支援によるコードの提案によって高度なDevSecOpsを実現できる方法
          link_text: 詳細はこちら
          href: https://about.gitlab.com/blog/2023/03/23/ai-assisted-code-suggestions/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: How AI-assisted code suggestions will advance DevSecOps
          data_ga_location: body
report_cta:
  layout: "dark"
  title: アナリストレポート
  reports:
  - description: "GitLabが2023年度第2四半期のForrester Wave™: Integrated Software Delivery Platformsにおける唯一のリーダーに認定"
    url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
  - description: "2023年 Gartner® DevOpsプラットフォームのMagic Quadrant™でGitLabがリーダーの1社に位置付け"
    url: /gartner-magic-quadrant/