title: スモールビジネス向けのスタートガイド
description: 競合優位性を築くには、他社よりも迅速に市場投入し、イノベーションを起こす必要があります。 このガイドでは、スモールビジネスに最適なソリューションであるGitLabの使用を開始する方法を説明します。
side_menu:
  anchors:
    text: このページの目次
    data:
    - text: サブスクリプションの選択
      href: "#getting-started"
      data_ga_name: getting-started
      data_ga_location: side-navigation
    - text: セットアップ
      href: "#get-setup"
      data_ga_name: getting-setup
      data_ga_location: side-navigation
    - text: GitLabを開始する
      href: "#utiliser-gitlab"
      data_ga_name: using-gitlab
      data_ga_location: side-navigation
hero:
  crumbs:
    - title: 今すぐ始める
      href: /get-started/
      data_ga_name: Get Started
      data_ga_location: breadcrumb
    - title: スモールビジネス向けのスタートガイド
  header_label: このページの推定読了時間：20分
  title: スモールビジネス向けのスタートガイド
  content: |
    競合優位性を築くには、他社よりも迅速に市場投入し、イノベーションを起こす必要があります。 複雑なDevSecOpsプロセスで遅れをとるわけにはいきません。 このガイドを活用すれば、Premiumプランでの自動化されたソフトウェア開発とデリバリーに必要な基本要素をすばやく設定できます。また、Ultimateプランで利用できるセキュリティ、コンプライアンス、プロジェクト計画など、オプションも豊富に組み込めます。
copy_info:
  title: 始める前に
  text: |
    GitLab 15.1 (2022年6月22日) 以降では、フリープランのGitLab.comのネームスペースは、<a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">ネームスペース</a>ごとに5人のメンバーに制限されます。 この制限は、最上位のグループと個人用ネームスペースに適用されます。 ユーザー数が多い場合は、有料版から始めることをおすすめします。
steps:
  groups:
    - header: サブスクリプションの選択
      show: すべて表示
      hide: すべて隠す
      id: getting-started
      items:
        - title: どのサブスクリプションが適切かを判断する
          copies:
            - title: GitLab SaaS版 または GitLab Self-Managed版
              text: |
                <p>GitLabプラットフォームをGitLabに管理してもらいたいですか、それともご自身で管理したいですか？</p>
              link:
                href: https://docs.gitlab.com/ee/subscriptions/#choose-between-gitlab-saas-or-gitlab-self-managed
                text: 違いを見る
                ga_name: GitLab Saas vs Self-Managed
                ga_location: body
        - title: どのプランが適切かを判断する
          copies:
            - title: フリー、Premium、Ultimate
              text: |
                <p>次の点を考慮して、どのプランがニーズに合っているかをご判断ください。</p>

            - title: ユーザー数
              text: |
                GitLabサブスクリプションは、SaaS版とSelf-Managed版の両方に対してコンカレント(ライセンス)モデルを使用します。 ユーザー/ライセンスの数は、プランの選択に影響を与えます。 ユーザーが５人を超える場合は、有料プラン (Premium または Ultimate) が必要です。
            - title: 必要なストレージ量
              text: |
                GitLab SaaS版 のフリー利用枠のネームスペースには、5GiBのストレージ制限があります。
            - title: 必要なセキュリティとコンプライアンス
              text: |
                <p>* シークレット検出、SAST、およびコンテナスキャンは、フリーとPremiumで利用できます。</p>
                <p>* DAST、依存関係、クラスターイメージ、IaC、API、ファジングなどの追加のスキャナー <a href="https://docs.gitlab.com/ee/user/application_security/"> は、Ultimateで利用できます。</a></p>
                <p>* マージリクエストパイプラインとセキュリティダッシュボードに統合された実用的な結果が必要な場合は、脆弱性管理が利用できるUltimateが必要です。</p>
                <p>* コンプライアンス パイプラインには Ultimate が必要です。</p>
                <p></a> * 当社の<a href="https://docs.gitlab.com/ee/user/application_security/">セキュリティスキャナ</a>および当社の<a href="https://docs.gitlab.com/ee/administration/compliance.html">コンプライアンス機能</a>についてご確認ください。
              link:
                href: /pricing/
                text: 詳細については、価格ページをご覧ください
                ga_name: pricing
                ga_location: body
    - header: セットアップ
      show: すべて表示
      hide: すべて隠す
      id: get-setup
      items:
        - title: SaaS版サブスクリプションのアカウントを設定する
          copies:
            - title: 必要なライセンス数を決定する
              text: |
                GitLab SaaSサブスクリプションは、コンカレント(ライセンス)モデルを使用します。 サブスクリプション期間中のユーザーの最大数に応じてサブスクリプションの料金を支払います。 合計ユーザーがサブスクリプション数を超えない限り、サブスクリプション期間中にいつでもユーザーを追加および削除できます。
              link:
                href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#how-seat-usage-is-determined
                text: ライセンス使用状況の決定方法に関する詳細
                ga_name: Determine how many seats you want
                ga_location: body
            - title: SaaS版サブスクリプションを購入する
              text: |
                <p>GitLab SaaSは、GitLabのサービスとしてのソフトウェア製品であり、GitLab.comで利用できます。GitLab SaaSを使用するために何もインストールする必要はなく、サインアップするだけで済みます。サブスクリプションによって、プライベート プロジェクトで使用できる機能が決まります。価格ページに移動し、[Premiumを購入] または [Ultimateを購入] を選択します。<p/>
                <p>パブリックオープンソースプロジェクトを持つ組織は、オープンソースプログラム用GitLabに積極的に申し込みできます。50,000ユニットのコンピューティングを含む <a href="/pricing/ultimate">GitLab Ultimate</a>の機能は、オープンソースプログラム用 <a href="/solutions/open-source">GitLabを通じてオープンソース</a> プロジェクトに無料で提供されます。</p>
              link:
                href: https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#obtain-a-gitlab-saas-subscription
                text: SaaS版サブスクリプションの詳細情報
                ga_name: Obtain your SaaS subscription
                ga_location: body
            - title: 必要なCI/CD共有ランナーのコンピューティング時間（分）を決定する
              text: |
                <a href="https://docs.gitlab.com/ee/ci/runners/runners_scope.html#shared-runners" data-ga-name="Shared Runners" data-ga-location="body">共有ランナー</a>は、GitLabインスタンス内のすべてのプロジェクトとグループで共有されます。ジョブが共有ランナーで実行されると、コンピューティング時間が使用されます。 GitLab.com では、コンピューティング時間（分）が <a href="https://docs.gitlab.com/ee/user/namespace/" data-ga-name="namespaces" data-ga-location="body">ネームスペース</a>ごとに設定され、 <a href="/pricing" data-ga-name="Your license tier" data-ga-location="body">ライセンスのプランによって決まります。</a><p/>
                <p>毎月決められたコンピューティング時間に加えて、GitLab.comでは、必要に応じて <a href="https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes\" data-ga-name=\"purchase additional compute minutes\" data-ga-location=\"body\">コンピューティング時間を追加購入できます</a>。</p>

        - title: Self-Managed版サブスクリプションのアカウントを設定する
          copies:
            - title: 必要なライセンス数を決定する
              text: |
                GitLab Self-Managedサブスクリプションは、コンカレント (ライセンス) モデルを使用します。 サブスクリプション期間中のユーザーの最大数に応じてサブスクリプションの料金を支払います。 合計ユーザーがサブスクリプション数を超えない限り、サブスクリプション期間中にいつでもユーザーを追加および削除できます。
              link:
                href: https://docs.gitlab.com/ee/subscriptions/self_managed/#subscription-seats
                text: ライセンスの決定方法に関する詳細
                ga_name: Determine how many seats you want
                ga_location: body
            - title: Self-Managed版サブスクリプションを購入する
              text: |
                ご自身でGitLabインスタンスをインストール、管理、およびメンテナンスできます。 価格ページに移動し、[Premiumを購入] または [Ultimateを購入] を選択します。
              link:
                href: https://docs.gitlab.com/ee/subscriptions/self_managed/
                text: Self_Managed版の詳細
                ga_name: Learn more about self-managed
                ga_location: body
            - title: GitLab Enterprise Editionをアクティベートする
              text: |
                ライセンスなしで新しいGitLabインスタンスをインストールすると、フリー機能のみが有効になります。 GitLab Enterprise Edition (EE) でより多くの機能を有効にするには、購入時に提供されたアクティベーションコードを使用してインスタンスをアクティベートします。 アクティベーションコードは、購入確認メールまたはカスタマーポータルの[購入の管理]で確認できます。
              link:
                href: https://docs.gitlab.com/ee/user/admin_area/license.html
                text: アクティベーションの詳細
                ga_name: Activate GitLab Enterprise Edition
                ga_location: body
            - title: システム要件を確認する
              text: |
                <p><a href="https://docs.gitlab.com/ee/install/requirements.html" data-ga-name="system rqeuirements" data-ga-location="body">サポートされているオペレーティングシステムと</a>、GitLabをインストールして使用するために必要な最小要件を確認してください。</p>
            - title: GitLabをインストールする
              text: |
                <p><a href="https://docs.gitlab.com/ee/install/#choose-the-installation-method" data-ga-name="Installation Method" data-ga-location="body">インストール方法</a>を選択してください</p>
                <p><a href="https://docs.gitlab.com/ee/install/#install-gitlab-on-cloud-providers" data-ga-name="your cloud provider" data-ga-location="body">クラウドプロバイダー</a> にインストールします。(該当する場合)</p>
            - title: インスタンスを設定する
              text: |
                通知用のメールアドレスをGitLabに接続したり、Docker Hubからコンテナイメージをキャッシュしてビルドを高速化できるように依存関係プロキシを設定したり、認証要件の決定といった設定が必要です。
              link:
                href: https://docs.gitlab.com/ee/install/next_steps.html
                text: 設定できる内容を確認する
                ga_name: Configure your self-managed instance
                ga_location: body
            - title: オフライン環境のセットアップ (オプション)
              text: |
                パブリックインターネットからの隔離が必要な場合、オフライン環境を設定します。(通常は規制対象の業界に適用可能)
              link:
                href: https://docs.gitlab.com/ee/user/application_security/offline_deployments/index.html
                text: オフライン環境が適しているかを確認してする
                ga_name: Set up off-line environment
                ga_location: body
            - title: CI/CD共有ランナーが使用可能なコンピューティング時間の制限を検討する
              text: |
                Self-Managed版 GitLabインスタンスのリソース使用率を制御するために、管理者は各ネームスペースのコンピューティング時間（分）を設定できます。
              link:
                href: https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#set-the-quota-of-cicd-minutes-for-a-specific-namespace
                text: さらに詳しく見る
                ga_name: Consider limiting CI/CD shared runner minutes allowed
                ga_location: body
            - title: GitLabランナーをインストールする
              text: |
                GitLabランナーは、GNU/Linux、macOS、FreeBSD、および Windowsにインストールして使用できます。 バイナリを手動でダウンロードするか、rpm / debパッケージのリポジトリを使用して、コンテナにインストールできます。
              link:
                href: https://docs.gitlab.com/runner/install/
                text: インストール オプションを確認する
                ga_name: Install GitLab runner
                ga_location: body
            - title: GitLabランナーを設定する (オプション)
              text: |
                GitLabランナーは、ご自身のニーズやポリシーに合わせて設定できます。
              link:
                href: https://docs.gitlab.com/runner/configuration/
                text: Sefl-Managedについて学ぶ
                ga_name: Configure GitLab runner
                ga_location: body
            - title: Self-Managed
              text: |
                Self-Managedにはご自身での管理が必要です。 管理者として、独自のニーズに合わせて様々な設定ができます。
              link:
                href: https://docs.gitlab.com/ee/administration/#configuring-gitlab
                text: Self-Managedについて学ぶ
                ga_name: Self Administration
                ga_location: body
        - title: アプリケーションの統合 (オプション)
          copies:
            - text: |
                シークレット管理や認証サービスなどの機能を追加したり、イシュートラッカーなどの既存のアプリケーションを統合できます。
              link:
                href: https://docs.gitlab.com/ee/integration/
                text: 統合について学ぶ
                ga_name: Integrate applications
                ga_location: body
    - header: GitLabを開始する
      show: すべて表示
      hide: すべて隠す
      id: utiliser-gitlab
      items:
        - title: 組織を設定する
          copies:
            - text: |
                組織とそのユーザーを設定します。 ユーザーの役割を決定し、必要なプロジェクトへのアクセス権を全員に付与します。
              link:
                href: https://docs.gitlab.com/ee/topics/set_up_organization.html
                text: さらに詳しく学ぶ
                ga_name: Setup your organization
                ga_location: body
        - title: プロジェクトで作業を整理する
          copies:
            - text: |
                GitLabでは、コードベースをホストするプロジェクトを作成できます。 また、プロジェクトを使用して、イシューの追跡、作業の計画、コードでの共同作業、継続的なビルド、テスト、組み込みのCI/CDの使用によるアプリのデプロイもできます。
              link:
                href: https://docs.gitlab.com/ee/user/project/index.html
                text: さらに詳しく学ぶ
                ga_name: Organize work with projects
                ga_location: body
        - title: 作業の計画と追跡
          copies:
            - text: |
                要件、イシュー、エピックを作成して作業計画を立てます。 マイルストーンを使用して作業をスケジュールし、チームの時間を追跡します。 クイックアクションで時間を節約する方法、GitLabがマークダウンテキストをレンダリングする方法、Gitを使用してGitLabと対話する方法を以下で学べます。
              link:
                href: https://docs.gitlab.com/ee/topics/plan_and_track.html
                text: さらに詳しく学ぶ
                ga_name: Plan and track work
                ga_location: body
        - title: アプリケーションをビルドする
          copies:
            - text: |
                ソースコードをリポジトリに追加し、マージリクエストを作成してコードをチェックインし、CI/CDを使用してアプリケーションを生成します。
              link:
                href: https://docs.gitlab.com/ee/topics/build_your_application.html
                text: さらに詳しく学ぶ
                ga_name: Build your application
                ga_location: body
        - title: アプリケーションを保護する
          copies:
            - title: 使用するスキャナーを決定する
              text: |
                GitLabは、シークレットの検出、SAST、コンテナスキャンをフリープラン利用枠で提供しています。 DAST、依存関係とIaCスキャン、APIセキュリティ、ライセンスコンプライアンス、ファジングは、Ultimateプランで利用できます。 すべてのスキャナーはデフォルトでオンになっていますが、オフに設定することもできます。
              link:
                href: https://docs.gitlab.com/ee/user/application_security/configuration/
                text: スキャナーの使用を構成する
                ga_name: Determine which scanners to use
                ga_location: body
            - title: セキュリティポリシーを構成する
              text: |
                GitLabのポリシーは、指定された構成に従ってプロジェクトパイプラインが実行されるたびに、セキュリティチームが選択したスキャンの実行を要求します。したがって、セキュリティチームは、設定したスキャンが変更、改ざん、無効化されていないことを確信できます。 スキャン実行とスキャン結果に対してポリシーを設定できます。
              link:
                href: https://docs.gitlab.com/ee/user/application_security/policies/
                text: セキュリティポリシーを構成する
                ga_name: Configure your security policies
                ga_location: body
            - title: マージリクエストの承認ルールを設定する
              text: |
                マージリクエストは、承認されない限りマージできないように設定できます。 GitLabフリー版では、開発者以上の権限を持つすべてのユーザーがマージリクエストを承認できますが、これらの承認はオプションです。 GitLab Premiumと￥GitLab Ultimateでは、より柔軟できめ細かい設定が可能です。 マージリクエストは、プロジェクト単位、グループ単位で承認できます。 GitLab PremiumおよびGitLab Ultimate、Self-Managed GitLabインスタンスの管理者は、インスタンス全体の承認を設定することもできます。
              link:
                href: https://docs.gitlab.com/ee/user/project/merge_requests/approvals/
                text: マージリクエスト承認ルールの詳細
                ga_name: Configure MR approval rules
                ga_location: body
        - title: アプリケーションのデプロイとリリース
          copies:
            - text: |
                アプリケーションを内部またはパブリックにデプロイします。 フラグを使用して、機能を段階的にリリースします。
              link:
                href: https://docs.gitlab.com/ee/topics/release_your_application.html
                text: さらに詳しく学ぶ
                ga_name: Deploy and release your application
                ga_location: body
        - title: アプリケーションのパフォーマンスを監視する
          copies:
            - text: |
                GitLabでは、アプリケーションの運用や保守に役立つさまざまなツールが利用できます。チームにとって最も重要なメトリックを追跡でき、パフォーマンスが低下した際に自動アラートを生成し、それらのアラートをすべてGitLab内で管理できるようになります。
              link:
                href: https://docs.gitlab.com/ee/operations/index.html
                text: さらに詳しく学ぶ
                ga_name: Monitor application performance
                ga_location: body
        - title: ランナーのパフォーマンスを監視する
          copies:
            - text: |
                GitLabには、独自のアプリケーションパフォーマンス測定システムがあります。GitLabパフォーマンスモニタリングにより、さまざまな統計を測定できます
              link:
                href: https://docs.gitlab.com/runner/monitoring/index.html
                text: さらに詳しく学ぶ
                ga_name: Monitor runner performance
                ga_location: body
        - title: インフラストラクチャを管理する
          copies:
            - text: |
                GitLabでは、インフラ管理を高速化し、シンプルにできるさまざまな機能が利用できます。
                GitLabは、クラウドインフラストラクチャのプロビジョニングのためにTerraformと緊密に統合されているため、セットアップなしですばやく開始できます。また、コード変更の場合と同じようにマージリクエストでインフラストラクチャの変更を共同作業し、モジュールレジストリを使用してスケーリングできます。
                GitLabとKubernetesの統合は、クラスターアプリケーションのインストール、構成、管理、デプロイ、トラブルシューティングを支援します。
              link:
                href: https://docs.gitlab.com/ee/user/infrastructure/index.html
                text: さらに詳しく学ぶ
                ga_name: Manage your infrastructure
                ga_location: body
        - title: GitLabの使用状況を分析する
          copies:
            - text: |
                GitLabは、プロジェクト、グループ、およびインスタンスレベルで分析できます。 DevOps調査や評価 (DORA) チームは、ソフトウェア開発チームのパフォーマンス指標として使用できる、いくつかの主要なメトリクスを開発しました。それらはGitLab Ultimateプランでご利用いただけます。
              link:
                href: https://docs.gitlab.com/ee/user/analytics/index.html
                text: さらに詳しく学ぶ
                ga_name: Analyze GitLab usage
                ga_location: body
next_steps:
  header: スモールビジネスの次なるステージへ
  cards:
    - title: カスタマーサポートが必要なお客様
      text: GitLab [documentation](https://docs.gitlab.com/){data-ga-name="documentation" data-ga-location="body"} をご覧いただき、疑問点の解決にお役立てください。
      avatar: /nuxt-images/icons/avatar_orange.png
      col_size: 4
      link:
        text: "カスタマーサポートの詳細はこちら"
        url: /support/
        data_ga_name: Contact support
        data_ga_location: body
    - title: さらに最適なサービスが必要なお客様
      text:  GitLabの導入からサードパーティアプリケーションの統合まで、GitLabプロフェッショナルサービスがサポートします。
      avatar: /nuxt-images/icons/avatar_pink.png
      col_size: 4
      link:
        text: お問い合わせはこちら
        url: /sales/
        data_ga_name: Have my PS contact me
        data_ga_location: body
    - title: チャネルパートナーにご相談したいお客様
      text: ディストリビューター、インテグレーター、またはマネージドサービスプロバイダー(MSP)にご相談も可能です。GitLabは、[クラウドパートナー](/partners/technology-partners/){data-ga-name="cloud partners" data-ga-location="body"}のマーケットプレイスを通じて購入することもできます。
      avatar: /nuxt-images/icons/avatar_blue.png
      col_size: 4
      link:
        text: チャネルパートナーディレクトリを見る
        url: https://partners.gitlab.com/English/directory/
        data_ga_name: See channel partner directory
        data_ga_location: body
    - title: アップグレードを検討しているお客様
      text: |
        [Premium](/pricing/premium/){data-ga-name="why premium" data-ga-location="body"}や[Ultimate](/pricing/ultimate/){data-ga-name="why ultimate" data-ga-location="body"} それぞれのメリットついての詳細は、以下のリンク先をご覧ください。
      col_size: 6
      link:
        text: Ultimateプランのメリット
        url: /pricing/ultimate
        data_ga_name: Why ultimate
        data_ga_location: body
    - title: サードパーティの統合を検討しているお客様
      text: GitLabのオープンコアにより、統合が容易になります。さまざまなニーズや事例を持つ、多くのテクノロジーパートナーが、GitLabの包括的なDevSecOpsプラットフォームを提供しています。
      col_size: 6
      link:
        text: アライアンスおよびテクノロジーパートナーについて詳しく読む
        url: /partners/technology-partners/
        data_ga_name: See our Alliance and Technology partners
        data_ga_location: body