---
  title: Integriere Kubernetes in deinen DevOps-Lebenszyklus
  description: Mit der Kubernetes-Integration von GitLab kannst du deine App in großem Umfang erstellen, testen, bereitstellen und ausführen.
  components:
    - name: 'solutions-hero'
      data:
        title: Kubernetes + GitLab
        subtitle: Alles, was du zum Erstellen, Testen, Bereitstellen und Ausführen deiner App im großen Maßstab brauchst
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          url: /webcast/scalable-app-deploy/
          text: Erfahre, wie GitLab dein Team bei der Skalierung der App-Bereitstellung unterstützen kann!
          data_ga_name: scale app deployment
          data_ga_location: header
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Bild: GitLab + Kubernetes"
    - name: copy-media
      data:
        block:
          - header: Die beste Lösung für Cloud Native Development
            miscellaneous: |
              Cloud-native Anwendungen sind die Zukunft der Softwareentwicklung. In Containern verpackt, dynamisch verwaltet und Microservice-orientiert, ermöglichen Cloud Native Systeme eine schnellere Entwicklungsgeschwindigkeit bei gleichzeitiger Stabilität des Betriebs.

              GitLab ist eine einzige Anwendung mit allem, was du für [End-to-End-Softwareentwicklung und -betrieb](/stages-devops-lifecycle/){data-ga-name="devops lifecycle" data-ga-location="body"} brauchst. Von der Ticket-Verfolgung und Quellcode-Verwaltung bis hin zu CI/CD und Monitoring – alles an einem Ort zu haben, vereinfacht die Komplexität der Toolchain und verkürzt die Zykluszeiten. Mit der [eingebauten Container-Registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html){data-ga-name="container registry" data-ga-location="body"} und der [Kubernetes-Integration](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="kubernetes integration" data-ga-location="body"} macht GitLab den Einstieg in die Containerisierung und die Cloud ⁠Native-Entwicklung so einfach wie nie zuvor und optimiert deine Entwicklungsprozesse für Cloud-Anwendungen.
          - header: Was ist Kubernetes?
            miscellaneous: |
              Kubernetes ist eine Open-Source-Plattform zur Container-Orchestrierung. Sie wurde entwickelt, um die Verwaltung von Anwendungscontainern von der Bereitstellung über die Skalierung bis hin zum Betrieb zu automatisieren. Die Kubernetes-Orchestrierung ermöglicht es dir, deine Container nach Bedarf zu partitionieren und hoch- und runterzuskalieren. So kannst du schnell und effizient auf Kundenwünsche reagieren, während du gleichzeitig die Hardwareauslastung in deiner Produktionsumgebung einschränkst und die Unterbrechung bei der Einführung neuer Funktionen minimierst.
            media_link_href: /blog/2017/11/30/containers-kubernetes-basics/
            media_link_text: Erfahre mehr über Kubernetes
            media_link_data_ga_name: more about kubernetes
            media_link_data_ga_location: body
          - header: Stelle GitLab auf Kubernetes bereit oder nutze GitLab, um deine Software auf Kubernetes zu testen und bereitzustellen
            miscellaneous: |
              Du kannst GitLab auf drei verschiedene Arten mit oder innerhalb von Kubernetes verwenden. Diese Arten können unabhängig voneinander oder zusammen genutzt werden.

              * [Software von GitLab in Kubernetes bereitstellen](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="deploy with kubernetes" data-ga-location="body"}
              * [Mit Kubernetes Runner verwalten, die an deine GitLab-Instanz angehängt sind](https://docs.gitlab.com/runner/install/kubernetes.html){data-ga-name="kubernetes runners" data-ga-location="body"}
              * [Ausführen von GitLab-Anwendung und -Diensten auf einem Kubernetes-Cluster](https://docs.gitlab.com/charts/){data-ga-name="kubernetes cluster" data-ga-location="body"}

              Jeder der oben beschriebenen Ansätze kann sowohl eigenständig als auch in Kombination mit den anderen verwendet werden. Zum Beispiel kann eine Omnibus-GitLab-Instanz, die auf einer virtuellen Maschine läuft, die darin gespeicherte Software über einen Docker-Runner in Kubernetes bereitstellen.
          - header: Kubernetes-Integration
            inverted: true
            text: |
              Mit GitLab kannst du deine Anwendungen fast überall einsetzen, von Bare Metal bis hin zu VMs, aber ursprüngliche wurde GitLab für Kubernetes konzipiert. Die [Kubernetes-Integration](https://docs.gitlab.com/ee/user/clusters/agent/){data-ga-name="kubernetes integration" data-ga-location="body"} ermöglicht dir den Zugriff auf erweiterte Funktionen wie:

              * [Pull-basierte Bereitstellungen](https://docs.gitlab.com/ee/user/clusters/agent/repository.html#synchronize-manifest-projects){data-ga-name="pull-based deployments" data-ga-location="body"}
              * [Bereitstellen von GitLab CI/CD über eine sichere Verbindung](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_tunnel.html){data-ga-name="CI/CD secure connection" data-ga-location="body"}
              * [Canary-Bereitstellungen](https://docs.gitlab.com/ee/user/project/canary_deployments.html){data-ga-name="canary deployments" data-ga-location="body"}
              * [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/){data-ga-name="auto devops" data-ga-location="body"}
            icon:
              use_local_icon: true
              name: /nuxt-images/logos/kubernetes.svg
              alt: Kubernetes
          - header: Nutze GitLab zum Testen und Bereitstellen deiner App auf Kubernetes
            text: |
              Mit [GitLab CI/CD](/features/continuous-integration/){data-ga-name="CI/CD" data-ga-location="body"} kannst du ganz einfach Bereitstellungen in mehreren Umgebungen verwalten. Führe automatisierte Tests parallel mit automatischer Skalierung durch [GitLab Runners](https://docs.gitlab.com/runner/){data-ga-name="gitlab runners" data-ga-location="body"}. Teste Änderungen manuell in einer produktionsnahen Umgebung, bevor du den Code mit Review Apps zusammenführst. Runners, [Reviews Apps](/stages-devops-lifecycle/review-apps/){data-ga-name="review apps" data-ga-location="body"} und deine eigene Anwendung können in deinem Kubernetes-Cluster oder einer anderen Umgebung deiner Wahl bereitgestellt werden.
            icon:
              name: rocket-alt
              alt: Rakete
              variant: marketing
              hex_color: '#F43012'
            link_href: /partners/technology-partners/google-cloud-platform/
            link_text: Bereitstellen auf der Google Cloud Platform
            link_data_ga_name: deploy on GCP
            link_data_ga_location: body
          - header: Verwende das gleiche Tool, das Kubernetes testet
            inverted: true
            text: |
              Prometheus, CoreDNS und Kubernetes selbst werden von [CNCF](https://www.cncf.io/) mit GitLab entwickelt, bereitgestellt und getestet. Sie setzen mehrere Projekte in mehreren Clouds ein und testen sie von Anfang bis Ende, indem sie GitLab Multiprojekt-Bereitstellungs-Boards verwenden, um den Fortschritt zu überwachen.
            video:
              video_url: https://www.youtube.com/embed/Jc5EJVK7ZZk?start=372&enablejsapi=1&origin=https%3A%2F%2Fabout.gitlab.com
            link_href: /customers/cncf/
            link_text: Mehr erfahren
            link_data_ga_name: cncf
            link_data_ga_location: body
    - name: featured-media
      data:
        column_size: 3
        horizontal_rule: true
        media:
          - title: Auto-DevOps-Dokumentation
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-devops.png
              alt: "DevOps"
            link:
              text: Mehr lesen
              href: https://docs.gitlab.com/ee/topics/autodevops/index.html
              data_ga_name: DevOps documentation
              data_ga_location: body
          - title: CI/CD-Pipeline mit Auto Deploy erstellen
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm.png
              alt: "Blog-Eintrag: CI/CD-Pipeline erstellen"
            link:
              text: Mehr lesen
              href: /blog/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/
              data_ga_name: CI/CD with auto deploy
              data_ga_location: body
          - title: GitLab auf Kubernetes installieren
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-gitlab-kubernetes.png
              alt: "Blog-Eintrag: CI/CD-Pipeline erstellen"
            link:
              text: Mehr lesen
              href: https://docs.gitlab.com/charts/
              data_ga_name: install gitlab on kubernetes
              data_ga_location: body
          - title: Cloud Native Webinar
            image:
              url: /nuxt-images/feature-thumbs/feature-thumb-webcast.png
              alt: "GitLab Webcast"
            link:
              text: Jetzt ansehen
              href: /blog/2017/04/18/cloud-native-demo/
              data_ga_name: cloud native webinar
              data_ga_location: body
