---
  title: À propos de GitLab
  description: En savoir plus sur GitLab et notre motivation.
  components:
    - name: 'solutions-hero'
      data:
        title: À propos de GitLab
        subtitle: Dans les coulisses de la plateforme DevSecOps
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        image:
          image_url: /nuxt-images/company/company_hero.png
          image_url_mobile: /nuxt-images/company/company_hero.png
          alt: "Les membres de l'équipe GitLab"
          bordered: true
          rectangular: true
    - name: 'copy-about'
      data:
        title: "Notre activité"
        subtitle: "Nous sommes la société derrière GitLab, la plateforme DevSecOps la plus complète."
        description: |
          Ce projet open source qui a commencé en 2011 afin d'aider une équipe de programmeurs à collaborer est devenu une plateforme utilisée par des millions de personnes pour livrer des logiciels plus rapidement et plus efficacement, tout en renforçant la sécurité et la conformité.
          \
          Depuis le début, nous croyons fermement au travail en distanciel, à l'open source, au DevSecOps et à l'itération. Nous nous levons et nous nous connectons le matin (ou à l'heure où nous choisissons de commencer) pour travailler aux côtés de la communauté GitLab afin de proposer chaque mois de nouvelles innovations qui aident les équipes à se concentrer sur une livraison plus rapide de codes, et non sur leur chaîne d'outils.
        cta_text: 'En savoir plus sur GitLab'
        cta_link: '/why-gitlab/'
        data_ga_name: "about gitlab"
        data_ga_location: "body"
    - name: 'copy-numbers'
      data:
        title: GitLab en chiffres
        rows:
          - item:
            - col: 4
              title: |
                **Contributeurs**
                **(code)**
              number: "3\_300+"
              link: http://contributors.gitlab.com/
              data_ga_name: contributors
              data_ga_location: body
            - col: 4
              title: |
                **Bureaux**
                Tout à distance depuis la création
              number: "0"
          - item:
            - col: 4
              title: |
                **Releases consécutives**
                le 22 de chaque mois
              number: "133"
              link: /releases/
              data_ga_name: releases
              data_ga_location: body
            - col: 4
              title: |
                **Membres de l'équipe**
                dans plus de 60 pays
              number: "1\_800+"
              link: /company/team/
              data_ga_name: team
              data_ga_location: body
          - item:
            - col: 8
              title: '**Nombre estimé d''utilisateurs enregistrés**'
              number: "Plus de 30\_millions"
        customer_logos_block:
          showcased_enterprises:
            - image_url: "/nuxt-images/home/logo_tmobile_white.svg"
              link_label: Lien vers la page d'accueil webcast de T-Mobile et GitLab"
              alt: "Logo T-Mobile"
              url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
            - image_url: "/nuxt-images/home/logo_goldman_sachs_white.svg"
              link_label: Lien vers l'étude de cas client Goldman Sachs
              alt: "Logo Goldman Sachs"
              url: "/customers/goldman-sachs/"
            - image_url: "/nuxt-images/home/logo_cncf_white.svg"
              link_label: Lien vers l'étude de cas client de la Cloud Native Computing Foundation
              alt: "Logo Cloud Native"
              url: /customers/cncf/
            - image_url: "/nuxt-images/home/logo_siemens_white.svg"
              link_label: Lien vers l'étude de cas client Siemens
              alt: "Logo Siemens"
              url: /customers/siemens/
            - image_url: "/nuxt-images/home/logo_nvidia_white.svg"
              link_label: Lien vers l'étude de cas client Nvidia
              alt: "Logo Nvidia"
              url: /customers/nvidia/
            - image_url: "/nuxt-images/home/logo_ubs_white.svg"
              link_label: Lien vers le post de blog Comment UBS a créé sa propre plateforme DevOps en utilisant GitLab
              alt: "Logo UBS"
              url: /blog/2021/08/04/ubs-gitlab-devops-platform/
        cta_title: |
          De la planification à la production,
          rassemblez les équipes dans une seule application
        cta_text: "Essai gratuit"
        cta_link: "https://gitlab.com/-/trial_registrations/new?glm_content=default-saas-trial&glm_source=about.gitlab.com"
        data_ga_name: "free trial"
        data_ga_location: "body"
    - name: 'copy-mission'
      data:
        title: "La mission de GitLab"
        description: "Notre mission est de faire en sorte que **tout le monde puisse contribuer**. Lorsque tout le monde peut contribuer, les utilisateurs deviennent des contributeurs. Nous augmentons ainsi considérablement les innovations."
        cta_text: 'En savoir plus'
        cta_link: '/company/mission/'
        data_ga_name: "mission"
        data_ga_location: "body"
    - name: 'showcase'
      data:
        title: Les valeurs de GitLab
        items:
          - title: Collaboration
            icon:
              name: collaboration-alt-4
              alt: Icône de collaboration
              variant: marketing
            text: "Nous donnons la priorité à tout ce qui nous aide à travailler efficacement ensemble, comme assumer une intention positive, dire «\_merci\_» et «\_pardon\_» et donner un feedback en temps opportun."
            link:
              href: /handbook/values/#collaboration
              data_ga_name: Collaboration
              data_ga_location: body
          - title: Résultats
            icon:
              name: increase
              alt: Icône d'augmentation
              variant: marketing
            text: "Nous opérons avec un sens de l'urgence et un parti pris pour l'action. Nous tenons les promesses que nous faisons les uns aux autres, aux clients, aux utilisateurs et aux investisseurs."
            link:
              href: /handbook/values/#results
              data_ga_name: results
              data_ga_location: body
          - title: Efficacité
            icon:
              name: digital-transformation
              alt: Icône d'efficacité
              variant: marketing
            text: "Qu'il s'agisse de choisir des solutions ennuyeuses, de tout documenter ou d'en gérer une, nous nous efforçons de progresser rapidement sur les bonnes choses."
            link:
              href: /handbook/values/#efficiency
              data_ga_name: efficiency
              data_ga_location: body
          - title: "Diversité, inclusion et appartenance"
            icon:
              name: community
              alt: Icône de communauté
              variant: marketing
            text: "Nous travaillons pour que GitLab soit un endroit où tout le monde, quelles que soient les origines ou les croyances, se sente à sa place et puisse s'épanouir."
            link:
              href: "/handbook/values/#diversity-inclusion"
              data_ga_name: diversity inclusion
              data_ga_location: body
          - title: Itération
            icon:
              name: continuous-delivery
              alt: Icône de livraison continue
              variant: marketing
            text: "Nous visons à rendre la plus petite chose viable et précieuse le plus rapidement possible pour obtenir un feedback."
            link:
              href: "/handbook/values/#iteration"
              data_ga_name: iteration
              data_ga_location: body
          - title: Transparence
            icon:
              name: open-book
              alt: Icône de livre
              variant: marketing
            text: "Tout ce que nous faisons est rendu public par défaut\_: de notre manuel d'entreprise aux gestionnaires de suivi des tickets pour notre produit."
            link:
              href: "/handbook/values/#transparency"
              data_ga_name: transparency
              data_ga_location: body
    - name: 'timeline'
      data:
        title: "GitLab au fil des années"
        items:
          - year: "2011"
            description: "Le projet GitLab a commencé par une validation"
          - year: ""
            description: "Nous avons commencé à sortir une nouvelle version de GitLab le 22 de chaque mois"
          - year: "2012"
            description: "La première version de GitLab CI est créée"
          - year: "2014"
            description: "GitLab est incorporé"
          - year: "2015"
            description: "Nous avons rejoint Y Combinator et publié le manuel GitLab dans notre dépôt de sites Web"
          - year: "2016"
            description: "Annonce de notre plan directeur et collecte de 20\_millions de dollars dans le cadre du cycle B"
          - year: "2021"
            description: "GitLab Inc. est devenue une société cotée en bourse sur le Nasdaq Global Market (NASDAQ\_: GTLB)"
    - name: 'copy-about'
      data:
        title: "Travailler chez GitLab"
        description: |
          Nous nous efforçons de créer un environnement en distanciel où tous les membres de l'équipe à travers le monde peuvent être eux-mêmes, contribuer de leur mieux, sentir qu'ils sont entendus et accueillis, et vraiment donner la priorité à l'équilibre entre le travail et la vie personnelle.
          \
          Si vous souhaitez faire partie de l'équipe, nous vous invitons à [en apprendre plus sur le travail chez GitLab](/jobs/) et à postuler pour tout poste vacant qui semble convenir.
        cta_text: 'Afficher tous les postes vacants'
        cta_link: '/jobs/all-jobs/'
        data_ga_name: "all jobs"
        data_ga_location: "body"
    - name: 'teamops'
      data:
        image: "/nuxt-images/company/company-teamops.svg"
        image_alt: "Collègues partageant des documents"
        mobile_img: "/nuxt-images/company/TeamOps-mobile.svg"
        header_img: "/nuxt-images/company/TeamOps.svg"
        title: "De meilleures équipes. Des progrès plus rapides. Un monde meilleur."
        description: "TeamOps est la pratique personnelle unique de GitLab qui fait du travail d'équipe une discipline objective. C'est ainsi que GitLab, qui était une start-up, est devenue une société publique mondiale en une décennie. Grâce à une certification de praticien gratuite et accessible, d'autres organisations peuvent tirer parti de TeamOps pour prendre de meilleures décisions, obtenir des résultats et faire avancer notre monde."
        button:
            link: "/teamops/"
            text: "En savoir plus sur TeamOps"
            data_ga_name: "learn more about teamops"
            data_ga_location: "body"
        link:
            link: "https://levelup.gitlab.com/learn/course/teamops"
            text: "Obtenir la certification"
            data_ga_name: "get certified"
            data_ga_location: "body"
    - name: 'learn-more-cards'
      data:
        column_size: 4
        cards:
          - icon:
              name: blog
              variant: marketing
              alt: Icône de blog
              hex_color: "#171321"
            event_type: "Blog"
            header: "Découvrez les nouveautés de GitLab, de DevOps, de la sécurité et plus encore."
            link_text: "Consultez le blog"
            href: "/blog/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: Logo GitLab
            data_ga_name: "blog"
            data_ga_location: "body"
          - icon:
              name: announcement
              variant: marketing
              alt: Icône d'annonce
              hex_color: "#171321"
            event_type: "Presse"
            header: "Nouveautés, communiqués de presse et notre dossier de presse."
            link_text: "En savoir plus"
            href: "/press/press-kit/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: Logo GitLab
            data_ga_name: "press kit"
            data_ga_location: "body"
          - icon:
              name: money
              variant: marketing
              alt: Icône d'argent
              hex_color: "#171321"
            event_type: "Relations avec les investisseurs"
            header: "Les dernières informations boursières et financières pour les investisseurs."
            link_text: "En savoir plus"
            href: "https://ir.gitlab.com/"
            image: "/nuxt-images/company/company-card-default.png"
            alt: Logo GitLab
            data_ga_name: "investor relations"
            data_ga_location: "body"

