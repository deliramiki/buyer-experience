---
  title: Tout en distanciel
  description: "GitLab est l'une des plus grandes entreprises mondiales en mode  tout distanciel"
  call_to_action:
    title:  Un leader mondial du télétravail
    shadow: true
    image_border: true
    subtitle: Découvrez comment nous avons réussi à nous développer en tant qu'entreprise asynchrone, sans bureau, et qui est constamment reconnue comme un endroit où il fait bon travailler.
    free_trial_button:
      text: Télécharger le playbook du travail en distanciel
      url: https://learn.gitlab.com/allremote/remote-playbook
      data_ga_name: Get the remote playbook
      data_ga_location: header
    contact_sales_button:
      text: Formations TeamOps
      url: /teamops/
      data_ga_name: TeamOps — making teamwork an objective discipline
      data_ga_location: header
    image:
      image_url: /nuxt-images/all-remote/remote-playbook-2023.png
      alt: Lien vers le playbook du travail en distanciel 2022
      image_href: https://learn.gitlab.com/allremote/remote-playbook
  feature_list:
    title: "Le guide complet du télétravail"
    subtitle: "Entièrement géré en tout distanciel depuis sa création, l'approche de la plateforme DevSecOps GitLab innove dans la façon dont les gens collaborent, communiquent et obtiennent des résultats dans plus de 65\_pays. Nos connaissances et nos meilleures pratiques sont ouvertes au public."
    icon:
      name: "remote-work-thin"
      alt: "Icône de télétravail"
      variant: marketing
    features:
      - title: "Les essentiels du télétravail"
        icon:
          name: "remote-world-alt"
          alt:
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: "Comparez\_: le tout distanciel vs. le mode hybride"
            url: /company/culture/all-remote/all-remote-vs-hybrid-remote-comparison/
            data_ga_location: 'body'
            data_ga_name: "Compare: All-Remote vs. Hybrid-Remote"
          - title: "Comment assurer la transition d'une entreprise vers le télétravail\_?"
            url: /company/culture/all-remote/transition/
            data_ga_location: 'body'
            data_ga_name: "company transition to remote operation"
          - title: "Les erreurs à ne pas commettre lors de la mise en place du télétravail"
            url: /company/culture/all-remote/what-not-to-do/
            data_ga_location: 'body'
            data_ga_name: "What not to do when implementing remote work"
      - title: "Concevoir une équipe à distance"
        icon:
          name: "remote-chat"
          alt:
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: Rôle du responsable du distanciel
            url: /company/culture/all-remote/head-of-remote/
          - title: Les 10 modèles d'équipes en distanciel
            url: /company/culture/all-remote/stages/
          - title: L'importance de la documentation pour les équipes travaillant en distanciel
            url: /company/culture/all-remote/handbook-first-documentation/
      - title: Embauche et intégration en distanciel
        icon:
          name: "user-laptop"
          alt: 
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: Embauche des membres d'une équipe en distanciel
            url: /company/culture/all-remote/hiring/
          - title: Le guide de l'intégration en distanciel
            url: /company/culture/all-remote/onboarding/
          - title: Évaluer une opportunité d'emploi en distanciel
            url: /company/culture/all-remote/evaluate/
      - title: Collaboration et asynchronisation
        icon:
          name: "idea-collaboration"
          alt: 
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: Comment travailler de manière asynchrone
            url: /company/culture/all-remote/asynchronous/
          - title: Organiser des réunions efficaces en distanciel
            url: /company/culture/all-remote/meetings/
          - title: Collaboration à distance et tableau blanc
            url: /company/culture/all-remote/collaboration-and-whiteboarding/
      - title: Mise à l'échelle de la culture du distanciel
        icon:
          name: "collaboration"
          alt: 
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: Comment créer une culture au sein d'une équipe travaillant en distanciel
            url: /company/culture/all-remote/building-culture/
          - title: L'importance de la communication informelle
            url: /company/culture/all-remote/informal-communication/
          - title: Établir une culture inclusive pour le travail en distanciel
            url: /company/culture/inclusion/building-diversity-and-inclusion/
      - title: En savoir plus
        icon:
          name: "handbook-gitlab"
          alt: 
          variant: marketing
        description: "Pour des ressources approfondies sur tout ce que vous devez savoir sur le télétravail, consultez le guide complet du travail en distanciel de GitLab."
        remove_bullet_points: true
        subfeatures:
          - title: Lire le guide complet du travail en distanciel
            url: /company/culture/all-remote/guide/
  highlights_header: Études de cas sur le travail à distance
  highlights_1:
    font_variant: 'lg'
    customers:
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-1.png"
          alt: ""
        url: "https://store.hbr.org/product/gitlab-and-the-future-of-all-remote-work-a/620066"
        impact: "GitLab et l'avenir du tout distanciel"
        logo: "/nuxt-images/all-remote/harvard_business_review_logo.svg"
        logo_alt: Logo de la Harvard Business Review
        stats:
          - qualifier: "Création de valeur du modèle du tout distanciel"
          - qualifier: "Processus organisationnels à grande échelle"
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-2.png"
          alt: ""
        logo: "/nuxt-images/all-remote/remote-logo-white.svg"
        logo_alt: Logo Remote
        url: "/customers/remote/"
        name: "Remote"
        impact: " respecte 100\_% de ses délais grâce à GitLab."
        stats:
          - qualifier: "Une seule source de vérité"
          - qualifier: "Pas de changement de contexte"
  highlights_2:
    font_variant: 'lg'
    customers:
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-3.png"
          alt: ""
        logo: "/nuxt-images/all-remote/insead.svg"
        logo_alt: Logo Insead
        url: "https://publishing.insead.edu/case/gitlab"
        impact: "GitLab\_: le travail en distanciel peut-il être étendu à 100\_% de l'entreprise\_?"
        stats:
          - qualifier: "Exemples de travail asynchrone"
          - qualifier: "Discussion sur la conception organisationnelle"
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-4.png"
          alt: ""
        logo: "/nuxt-images/all-remote/hotjar_logo.svg"
        logo_alt: Logo Hotjar
        url: "/customers/hotjar/"
        impact: "L'équipe Hotjar qui travaille qu'en distanciel déploie ses solutions 50\_% plus rapidement grâce à GitLab."
        stats:
          - qualifier: "Visibilité de bout en bout"
          - qualifier: "Opérations en tout distanciel"
  events:
    hide_horizontal_rule: true
    icon:
      name: ribbon-check-alt-thin
      alt: Icône de ruban avec une coche
      variant: marketing
    header: Obtenez une certification de télétravail
    cards_per_row: 2
    description: Devenez un expert en compétences essentielles pour réussir dans un environnement de travail distribué.
    events:
      - heading: Comment gérer une équipe travaillant en distanciel
        event_type: Formation
        destination_url: 'https://www.coursera.org/learn/remote-team-management'
        button_text: Détails de l'évènement
      - heading: Formation sur les bases du distanciel
        event_type: Formation
        destination_url: https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge
        button_text: Détails de l'évènement
  resources_tabs:
    title: Ressources associées
    align: left
    column_size: 4
    grouped: true
    cards:
      - icon:
          name: blog
          variant: marketing
          alt: Blog Icon
        event_type: "Blog"
        header: 5 façons de généraliser le travail en distanciel au sein de votre équipe
        link_text: "Apprendre encore plus"
        href: /blog/2021/08/09/five-ways-to-scale-remote-work/
        data_ga_name: "5 ways to scale remote work on your team"
        data_ga_location: resource cards
      - icon:
          name: blog
          variant: marketing
          alt: Blog Icon
        event_type: "Blog"
        header: "Conseils pour la gestion d'équipes d'ingénieurs travaillant en distanciel"
        link_text: "Apprendre encore plus"
        image: "/nuxt-images/resources/resources_1.jpeg"
        href: /blog/2021/01/29/tips-for-managing-engineering-teams-remotely/
        data_ga_name: "tips for managing remote working engineering teams"
        data_ga_location: resource cards
      - icon:
          name: blog
          variant: marketing
          alt: Blog Icon
        event_type: "Blog"
        header: "Comment nous réalisons le travail UX et de Design à distance"
        link_text: "Apprendre encore plus"
        image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
        href: /blog/2020/03/27/designing-in-an-all-remote-company/
        data_ga_name: "how we carry out remote work UX and design"
        data_ga_location: resource cards    
      - icon:
          name: video
          variant: marketing
          alt: 
        event_type: "Vidéo"
        header: "Pourquoi travailler en distanciel\_?"
        link_text: Regarde maintenant
        image: "/nuxt-images/all-remote/2.jpg"
        href: https://www.youtube-nocookie.com/embed/GKMUs7WXm-E
        data_ga_name: why work remotely
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 
        event_type: "Vidéo"
        header: "Tout ce qu'il y a à savoir sur la mécanique du distanciel"
        link_text: Regarde maintenant
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-webcast-the-mechanics.jpg
        href: https://www.youtube-nocookie.com/embed/9xvVMglCm6I
        data_ga_name: The Mechanics of All Remote
        data_ga_location: resource cards     
      - icon:
          name: video
          variant: marketing
          alt: 
        event_type: "Vidéo"
        header: "Prévenir l'épuisement professionnel et trouver l'équilibre"
        link_text: Regarde maintenant
        image: "/nuxt-images/all-remote/vid-thumb-universal-remote-preventing-burnout.jpg"
        href: https://www.youtube-nocookie.com/embed/Q9yjo6IOqX4
        data_ga_name: Preventing Burnout and Achieving Balance
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 
        event_type: "Vidéo"
        header: "Repenser la gestion à distance"
        link_text: Regarde maintenant
        image: "/nuxt-images/all-remote/vid-thumb-universal-remote-rethink-remote-management.jpg"
        href: https://www.youtube-nocookie.com/embed/dsnSb-lpZSI
        data_ga_name: Rethinking Remote Management
        data_ga_location: resource cards 
      - icon:
          name: video
          variant: marketing
          alt:
        event_type: "Vidéo"
        header: "Conseils pour une main-d'œuvre productive à distance"
        link_text: Regarde maintenant
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-tips-for-a-productive-workforce.jpg
        href: https://www.youtube-nocookie.com/embed/CsLswGz6J5s
        data_ga_name: Tips for a productive all-remote workforce
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 
        event_type: "Vidéo"
        header: "Sid et Dominic discutent du tout distanciel"
        link_text: Regarde maintenant
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-ceo-dominic-monn.jpg
        href: https://www.youtube-nocookie.com/embed/EeUhxQn_ct4
        data_ga_name: Sid and Dominic discuss all remote work
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt:
        event_type: "Vidéo"
        header: "Gérer des équipes exceptionnelles"
        link_text: Regarde maintenant
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-managing-exceptional-teams.jpg
        href: https://www.youtube-nocookie.com/embed/6VVAsMEKqFU
        data_ga_name: Managing Exceptional Teams
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 
        event_type: "Vidéo"
        header: "Faire évoluer une culture d'entreprise tout en distanciel"
        link_text: Regarde maintenant
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-fireside-chat.jpg
        href: https://www.youtube-nocookie.com/embed/hVUzXmjKQJ0
        data_ga_name: Evolving a Fully Remote Company Culture
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 
        event_type: "Vidéo"
        header: "Utiliser la vidéo pour une collaboration efficace"
        link_text: Regarde maintenant
        image: /nuxt-images/all-remote/vid-thumb-universal-using-video-collaboration.jpg
        href: https://www.youtube-nocookie.com/embed/6mZqzK_40FE
        data_ga_name: Using video for effective collaboration
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt:
        event_type: "Rapport"
        header: 'Le rapport du travail en distanciel'
        link: /
        link_text: "Apprendre encore plus"
        href: /company/culture/all-remote/remote-work-report
        data_ga_name: 'The remote work report'
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt: 
        event_type: "Rapport"
        header: 'Guide pratique du télétravail'
        link_text: "Apprendre encore plus"
        href: /company/culture/all-remote/work-from-home-field-guide/
        data_ga_name: 'work from home guide'
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt: 
        event_type: "Rapport"
        header: 'Hors du bureau'
        link_text: "Apprendre encore plus"
        href: /company/culture/all-remote/out-of-the-office
        data_ga_name: 'out of the office'
        data_ga_location: resource cards