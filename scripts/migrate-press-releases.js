/* eslint @typescript-eslint/no-var-requires: 0 */
/* eslint no-console: 0 */
const yaml = require('js-yaml');
const fs = require('fs');
const contentful = require('contentful-management');
const process = require('process');
const pressFiles =
  '/Users/mateofernandopenagos/Documents/gitlab/www-gitlab-com/sites/uncategorized/source/press/releases'; // press files folder in www to use as migration target
const accessToken = process.argv[2]; // CMA access token
const spaceId = process.argv[3]; // About.gitlab.com space id

async function migratePressReleases(auth, startFrom) {
  const client = contentful.createClient({
    accessToken: auth.accessToken,
  });
  const environment = await client
    .getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env));
  let parsedPress = parseToJSON(pressFiles);
  if (startFrom) {
    const startIndex = parsedPress.findIndex(
      (post) => post.title === startFrom,
    );
    console.log('starting from post at index ', startIndex);
    parsedPress = parsedPress.splice(startIndex, parsedPress.length - 1);
  }

  // const testEntry = [ parsedPress[50]]
  for (const [index, post] of parsedPress.entries()) {
    //  Create required entries for the press release post:
    //  Create text blocks
    const textBlockIdArray = await createTextBlocks(post, environment);
    // SEO Metadata
    const SEOId = await createSEOMetaData(post, environment);
    // Create page
    await createCustomPage(post, textBlockIdArray, SEOId, environment);
    // await createBlogPost(post, assetId, authorId, categoryId, environment)
    console.log(`PROGRESS: ${Math.round((index / parsedPress.length) * 100)}%`);
  }
}

async function createCustomPage(post, idArray, SEOId, environment) {
  const contentType = 'customPage';
  const [path] = post.path.split('.');
  const slug = post.canonical_path || `${path}.html`;

  console.log(
    `creating custom page for ${slug} and linking ${idArray.length} text blocks`,
  );

  const [year, month, day] = post.path.split('-');
  const date = `${year}-${month}-${day}`;
  const linkedEntries = idArray.map((id) => ({
    sys: {
      type: 'Link',
      linkType: 'Entry',
      id,
    },
  }));
  await environment.createEntry(contentType, {
    fields: {
      pageName: {
        'en-US': post.title,
      },
      header: {
        'en-US': post.title,
      },
      description: {
        'en-US': post.description,
      },
      date: {
        'en-US': date,
      },
      slug: {
        'en-US': `/press/releases${
          slug.startsWith('/') ? `${slug}/` : `/${slug}/`
        }`,
      },
      seoMetaData: {
        'en-US': {
          sys: {
            type: 'Link',
            linkType: 'Entry',
            id: SEOId,
          },
        },
      },
      components: {
        'en-US': linkedEntries,
      },
      activateSideMenu: {
        'en-US': true,
      },
    },
  });
  console.log('custom page created');
}

async function createSEOMetaData(post, environment) {
  const contentType = 'seo';
  console.log(`creating SEO metadata`);
  try {
    const textBlock = await environment.createEntry(contentType, {
      fields: {
        title: {
          'en-US': post.title,
        },
        description: {
          'en-US': post.description,
        },
        ogTitle: {
          'en-US': post.title,
        },
      },
    });
    console.log('Created SEO metadata');
    return textBlock.sys.id;
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message);
    console.log(parsedError.message);
  }
}

async function createTextBlocks(post, environment) {
  const textBlockArray = separateTextBlocks(post.markdown);
  const idArray = [];
  for (const [index, block] of textBlockArray.entries()) {
    const blockEntry = await createTextBlockEntry(
      block,
      `${post.title}-${index}`,
      environment,
    );
    idArray.push(blockEntry);
  }
  return idArray;
}

async function createTextBlockEntry(block, internalName, environment) {
  const contentType = 'textBlock';
  console.log(`creating text block`);
  try {
    const textBlock = await environment.createEntry(contentType, {
      fields: {
        internalName: {
          'en-US': internalName,
        },
        header: {
          'en-US': block.header,
        },
        text: {
          'en-US': block.text,
        },
      },
    });
    console.log('Created text Block');
    return textBlock.sys.id;
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message);
    console.log(parsedError.message);
  }
}

function separateTextBlocks(markdown) {
  const parts = markdown.split(/\*\*([^*]+)\*\*/).filter(Boolean);
  const boldedIndexes = [];
  for (let i = 1; i < parts.length; i += 2) {
    if (parts[i]) {
      boldedIndexes.push(i);
    }
  }

  boldedIndexes.forEach((index) => {
    console.log(parts[index]);
  });
  const result = boldedIndexes.map((boldIndex) => ({
    header: parts[boldIndex],
    text: parts[boldIndex + 1],
  }));

  if (parts[0].length) {
    result.unshift({ header: null, text: parts[0] });
  }
  return result;
}

function parseToJSON(pressPath) {
  console.log('parsing press files to JSON');
  const pressReleases = fs.readdirSync(pressPath);
  const parsedReleases = pressReleases
    .map((file) => {
      const [__, yml, markdown] = fs
        .readFileSync(`${pressPath}/${file}`, { encoding: 'utf8', flag: 'r' })
        .split('---');

      const parsedYML = yaml.load(yml, { json: true });
      // Make relative images absolute since the markdown references assets that are not in the CMS
      const updatedMarkdown = makeImagePathsAbsolute(markdown);
      return { ...parsedYML, path: file, markdown: updatedMarkdown };
    })
    .filter((file) => file);

  console.log(`Successfully parsed ${parsedReleases.length} press files`);

  return parsedReleases;
}

function makeImagePathsAbsolute(markdownText) {
  const regex = /(!\[.*?\]\()(\/[^)]+)\)/g;

  const prefix = 'https://about.gitlab.com';

  const newMarkdownText = markdownText.replace(regex, `$1${prefix}$2)`);

  return newMarkdownText;
}

migratePressReleases({
  accessToken,
  spaceId,
  env: 'master',
});
