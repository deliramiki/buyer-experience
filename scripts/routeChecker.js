const expectedRoutes = [
  '/',
  '/pricing',
  '/why-gitlab',
  '/features',
  '/install',
  '/free-trial/devsecops',
  '/platform',
  '/free-trial',
  '/software-faster',
  '/gitlab-duo',
  '/support',
  '/support/us-government-support',
  '/support/statement-of-support',
  '/support/providing-large-files',
  '/support/general-policies',
  '/support/customer-satisfaction',
  '/support/definitions',
  '/support/scheduling-upgrade-assistance',
  '/support/sensitive-information',
  '/support/managing-support-contacts',
  '/support/portal',
  '/support/gitlab-com-policies',
  '/learn',
  '/features',
  '/solutions',
];

const areCriticalRoutesGenerated = (generatedRoutes) => {
  return expectedRoutes.every((expectedRoute) => {
    // eslint-disable-next-line no-console
    console.log(`Checking: ${expectedRoute}`);
    return generatedRoutes.has(expectedRoute);
  });
};

export { areCriticalRoutesGenerated };
